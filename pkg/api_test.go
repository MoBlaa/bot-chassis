package pkg

import (
	"encoding/json"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"testing"
)

func TestApi_JsonApiResponse(t *testing.T) {
	apiResponse := &HTTPResponse{
		Code: 15,
		Body: config.Config{},
	}

	data, err := json.Marshal(apiResponse)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(string(data))
}

package client

import (
	"strings"
)

// Parse converts a string formatted as IRCv3-Message to a TwitchMessage.
func Parse(input string) *TwitchMessage {
	// Every step of extraction parses its part if present
	// and then stores the remaining input to parse into 'remains'

	// Get Tags
	remains := input
	tags := make(map[string]string)
	if strings.HasPrefix(input, "@") {
		cursor := strings.Index(remains, " ")
		strTags := remains[1:cursor]
		remains = remains[cursor+1:]
		tags = ParseTags(strTags)
	}

	// Get Prefix
	var prefix *Prefix
	if strings.HasPrefix(remains, ":") {
		cursor := strings.Index(remains, " ")
		pre := remains[1:cursor]
		remains = remains[cursor+1:]
		prefix = ParsePrefix(pre)
	}

	// Get Command
	cmdEnd := strings.Index(remains, " ")
	var command string
	if cmdEnd == -1 {
		command = remains
		remains = ""
	} else {
		command = remains[:cmdEnd]
		remains = remains[cmdEnd:]
	}

	// Parse params
	var params []string
	var message string
	if remains != "" {
		params, message = ParseParams(remains)
	}

	return &TwitchMessage{
		Tags:    tags,
		Prefix:  prefix,
		Command: command,
		Params:  params,
		Message: message,
	}
}

// ParseTags creates a Map of Key-Value-Tags from a string with
// format: "@key1=value;key2=value;key3=randomValue".
// Ignores badly formatted key-value pairs and extracts well formatted ones.
func ParseTags(input string) map[string]string {
	if strings.HasPrefix(input, "@") {
		input = input[1:]
	}
	input = strings.Trim(input, " ")

	tags := make(map[string]string)
	split := strings.Split(input, ";")
	for _, sp := range split {
		keyVal := strings.Split(sp, "=")
		if len(keyVal) == 2 {
			tags[keyVal[0]] = keyVal[1]
		}
	}
	return tags
}

// ParsePrefix parses strings formatted as ":?name((!user)?@host)".
func ParsePrefix(input string) *Prefix {
	start := 0
	if strings.HasPrefix(input, ":") {
		start = 1
	}
	hasUser := strings.Contains(input, "!")
	hasHost := strings.Contains(input, "@")

	var prefix *Prefix
	if hasUser && hasHost {
		nameEnd := strings.Index(input, "!")
		userEnd := strings.Index(input, "@")

		prefix = &Prefix{
			Name: input[start:nameEnd],
			User: input[nameEnd+1 : userEnd],
			Host: input[userEnd+1:],
		}
	} else if hasHost {
		nameEnd := strings.Index(input, "@")

		prefix = &Prefix{
			Name: input[start:nameEnd],
			Host: input[nameEnd+1:],
		}
	} else {
		if hasUser {
			prefix = &Prefix{
				Name: input[start:strings.Index(input, "!")],
			}
		} else {
			prefix = &Prefix{
				Name: input[start:],
			}
		}
	}
	return prefix
}

// ParseParams converts input to slice of parameters.
// Expects input to contain the string directly after the command.
func ParseParams(input string) (params []string, message string) {
	index := strings.Index(input, " :")
	var strParams string
	if index == -1 {
		strParams = input
	} else {
		strParams = input[:index]
		message = input[index+2:]
	}
	strParams = strings.Trim(strParams, " ")
	if strParams != "" {
		params = strings.Split(strParams, " ")
	}
	return
}

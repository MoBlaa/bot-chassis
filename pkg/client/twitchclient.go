package client

import (
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain/roles"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"gitlab.com/MoBlaa/bot-chassis/pkg/events"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"strconv"
	"strings"
	"time"
)

// TwitchClient contains information about the connection to Twitch.
type TwitchClient struct {
	username string
	conn     *websocket.Conn
}

// New creates new TwitchClient which can be used to listen and send messages to/from Twitch.
// Client is closed if message input is closed.
func New(cfg *config.ConnectionConfig) (*TwitchClient, error) {
	// Connect to Twitch Websocket-Server
	conn, _, err := websocket.DefaultDialer.Dial(cfg.URL.String(), nil)
	if err != nil {
		elog.Fatal(fmt.Errorf("failed to connecto to Twitch: %w", err))
	}
	cl := &TwitchClient{
		username: cfg.Username,
		conn:     conn,
	}

	mssgs := []*TwitchMessage{
		{
			Command: "PASS",
			Params:  []string{cfg.Token},
		},
		{
			Command: "NICK",
			Params:  []string{cfg.Username},
		},
		{
			Command: "CAP",
			Params:  []string{"REQ"},
			Message: "twitch.tv/membership",
		},
		{
			Command: "CAP",
			Params:  []string{"REQ"},
			Message: "twitch.tv/tags",
		},
		{
			Command: "CAP",
			Params:  []string{"REQ"},
			Message: "twitch.tv/commands",
		},
		//fmt.Sprintf("PASS %s", oauthToken),
		//fmt.Sprintf("NICK %s", username),
		//fmt.Sprintf("CAP REQ :twitch.tv/membership"),
		//fmt.Sprintf("CAP REQ :twitch.tv/tags"),
		//fmt.Sprintf("CAP REQ :twitch.tv/commands"),
	}
	for _, channel := range cfg.ChannelConfig.JoinedChannels {
		mssgs = append(mssgs, &TwitchMessage{
			Command: "JOIN",
			Params:  []string{fmt.Sprintf("#%s", channel)},
		})
	}
	elog.Info(fmt.Sprintf("Logging in as '%s'", cfg.Username))

	// Initialize Connection with login and requesting capabilites (Tags, Memberships, etc.)
	for _, initMssg := range mssgs {
		err = send(conn, initMssg)
		if err != nil {
			return nil, err
		}
	}

	// Wait for responses to auth, join and CAP REQ
	awaited := []*TwitchMessage{
		{Command: "001"}, // Welcome, GLHF
		{Command: "002"}, // Your host ...
		{Command: "003"}, // this server is rather new
		{Command: "004"}, // -
		{Command: "375"}, // -
		{Command: "372"}, // You are in a maze of twisty passages, all alike.
		{Command: "376"}, // >
		{Command: "CAP", Params: []string{"*", "ACK"}, Message: "twitch.tv/membership"},
		{Command: "CAP", Params: []string{"*", "ACK"}, Message: "twitch.tv/tags"},
		{Command: "CAP", Params: []string{"*", "ACK"}, Message: "twitch.tv/commands"},
	}
	for _, channel := range cfg.ChannelConfig.JoinedChannels {
		awaited = append(awaited, &TwitchMessage{
			Command: "JOIN",
			Params: []string{
				fmt.Sprintf("#%s", channel),
			},
		}, &TwitchMessage{
			Command: "353",
			Params: []string{
				cfg.Username,
				"=",
				fmt.Sprintf("#%s", channel),
			},
			Message: cfg.Username,
		}, &TwitchMessage{
			Command: "366",
			Params: []string{
				cfg.Username,
				fmt.Sprintf("#%s", channel),
			},
		})
	}

	err = <-cl.WaitForMessages(awaited, 5*time.Second)

	if err == nil {
		// Send connection successful event
		elog.Log(elog.INFO, events.EventIDConnectionSuccess, fmt.Sprintf("Successfully established connection as '%s'", cfg.Username))
	}

	return cl, err
}

// WaitForMessages reads with TwitchClient as long as not all expected
// messages has been received or the timeout is reached.
func (cl *TwitchClient) WaitForMessages(expected []*TwitchMessage, timeout time.Duration) <-chan error {
	out := make(chan error)

	go func() {
		defer close(out)
		// Prepare map for unordered responses
		mssgCount := 0
		seen := make(map[int]bool)

		for {
			select {
			case <-time.NewTimer(timeout).C:
				out <- errors.New("timeout waiting for Messages")
				return
			case <-time.NewTimer(time.Millisecond).C:
				_, message, err := cl.conn.ReadMessage()
				if err != nil {
					elog.Info(fmt.Sprintf("> Error: %v\n", err))
					out <- err
					return
				}
				strMssg := string(message)
				mssgs := strings.Split(strings.ReplaceAll(strMssg, "\r\n", "\n"), "\n")
				for _, single := range mssgs {
					if strings.ReplaceAll(single, " ", "") == "" {
						continue
					}

					// Parse incoming messages and write to output
					mssg := Parse(single)

					// Also add to log
					elog.Log(elog.INFO, events.EventIDChatMessage, single)

					// Iterate over expected to check against
					for i, awaited := range expected {
						if !seen[i] && awaited.Eq(mssg) {
							mssgCount++
							seen[i] = true
						}
					}
				}

				if mssgCount >= len(expected) {
					return
				}
			}
		}
	}()

	return out
}

// Listen starts listening on messages from twitch.
func (cl *TwitchClient) Listen() <-chan *messages.Event {
	out := make(chan *messages.Event)

	go func() {
		defer close(out)
		for {
			_, message, err := cl.conn.ReadMessage()
			if err != nil {
				// Removed as triggered every time the connection is closed
				//elog.Error(fmt.Errorf("error reading message from twitch: %w", err))
				elog.Debug(fmt.Sprintf("Closing listener for twitch messages!"))
				return
			}
			strMssg := string(message)
			mssgs := strings.Split(strings.ReplaceAll(strMssg, "\r\n", "\n"), "\n")
			for _, single := range mssgs {
				if strings.ReplaceAll(single, " ", "") == "" {
					continue
				}

				// Parse incoming messages and write to output
				mssg := Parse(single)
				if mssg == nil {
					elog.Fatal(fmt.Errorf("failed to parse incoming message: %v", single))
				}
				elog.Log(elog.INFO, events.EventIDChatMessage, mssg)

				// React to PING messages
				if strings.ToLower(mssg.Command) == "ping" {
					err = send(cl.conn, &TwitchMessage{
						Command: "PONG",
						Message: mssg.Message,
					})
					if err != nil {
						elog.Fatal(fmt.Errorf("failed to send message to twitch: %w", err))
					}
				}

				var tp transport.Transport
				var target string
				if strings.ToLower(mssg.Command) == "whisper" {
					tp = transport.WHISPER
					target = mssg.Params[0]
				} else if strings.ToLower(mssg.Command) == "privmsg" {
					// Otherwise channel messages
					tp = transport.CHANNEL
					// Also remove '#'
					target = mssg.Params[0][1:]
				} else {
					continue
				}
				// Parse user id
				id, err := strconv.Atoi(mssg.Tags["user-id"])
				if err != nil {
					elog.Error(fmt.Errorf("error parsing user-id '%d': %v", id, err))
					id = 0
				}
				// Get roles
				var rls []roles.UserRole
				badges := strings.ToLower(mssg.Tags["badges"])
				if strings.Contains(badges, string(roles.Moderator)) {
					rls = append(rls, roles.Moderator)
				}
				if strings.Contains(badges, string(roles.Streamer)) {
					rls = append(rls, roles.Streamer)
				}
				if strings.Contains(badges, string(roles.Subscriber)) {
					rls = append(rls, roles.Subscriber)
				}
				out <- &messages.Event{
					Transport: tp,
					Target:    target,
					Sender: &domain.User{
						ID:   domain.ID(id),
						Name: mssg.Prefix.User,
					},
					Message: mssg.Message,
				}
			}
		}
	}()

	return out
}

func send(conn *websocket.Conn, mssg *TwitchMessage) error {
	elog.Info(fmt.Sprintf("sending message: %v", mssg))
	return conn.WriteMessage(websocket.TextMessage, []byte(mssg.String()))
}

// StartSender writes messages to twitch and quits if `done` or `in` are closed.
func (cl *TwitchClient) StartSender(done <-chan struct{}, in <-chan *messages.Message) {
	go func() {
		defer elog.Log(elog.INFO, events.EventIDBotStopped, "Stopped the bot!")
		// Dial outgoing messages
		for {
			select {
			case input, more := <-in:
				if input != nil {
					cl.sendMessage(input)
				}
				if !more {
					return
				}
			case _, more := <-done:
				if !more {
					return
				}
			}
		}
	}()
}

// sendMessage sends a message to twitch by converting it to one or many TwitchMessages
// and writing it to the twitch websocket connection.
func (cl *TwitchClient) sendMessage(input *messages.Message) {
	var mssg *TwitchMessage
	// Split into length of maximum whisper length
	splitLength := 500 - len(input.Target) - 3
	var limitedMessages []string
	if len(input.Message) > splitLength {
		// Split message by maximum size of message to send
		// but don't split in the middle of a word if possible
		rest := input.Message
		for len(rest) > 0 {
			// get next message
			var tmp string
			if len(rest) >= splitLength {
				tmp = rest[:splitLength]
				rest = rest[splitLength:]
			} else {
				tmp = rest
				rest = ""
			}
			// Assure not splitting in the middle of a word
			lastSpace := strings.LastIndex(tmp, " ")
			if lastSpace != -1 {
				rest = tmp[lastSpace+1:] + rest
				tmp = tmp[:lastSpace]
			}
			limitedMessages = append(limitedMessages, tmp)
		}
	} else {
		limitedMessages = append(limitedMessages, input.Message)
	}
	for _, limited := range limitedMessages {
		if input.Transport == transport.CHANNEL {
			mssg = &TwitchMessage{
				Command: "PRIVMSG",
				Params: []string{
					fmt.Sprintf("#%s", input.Target),
				},
				Message: limited,
			}
		} else if input.Transport == transport.WHISPER {
			mssg = &TwitchMessage{
				Command: "PRIVMSG",
				Params: []string{
					fmt.Sprintf("#%s", cl.username),
				},
				Message: fmt.Sprintf("/w %s %s", input.Target, limited),
			}
		}
		if mssg != nil {
			elog.Info(fmt.Sprintf("sending message: %v", mssg))
			err := cl.conn.WriteMessage(websocket.TextMessage, mssg.Bytes())
			if err != nil {
				elog.Fatal(fmt.Errorf("error sending message: %w", err))
			}
		}
	}
}

// Close closes the connection to twitch.
func (cl *TwitchClient) Close() error {
	return cl.conn.Close()
}

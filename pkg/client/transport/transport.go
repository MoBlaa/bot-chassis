package transport

// Transport represents the way messages and events are sent.
type Transport int

const (
	// WHISPER marks a Message sent through twitch whispers.
	WHISPER Transport = 1
	// CHANNEL marks a Message sent to a twitch channel.
	CHANNEL Transport = 2
)

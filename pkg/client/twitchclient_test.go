package client

import (
	"context"
	"fmt"
	"github.com/gorilla/websocket"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
	"log"
	"net/http"
	"net/url"
	"strings"
	"testing"
	"time"
)

var (
	Welcome001       = &TwitchMessage{Command: "001", Message: "Welcome, GLHF!"}
	Welcome002       = &TwitchMessage{Command: "002", Message: "Your host is twitch.tv"}
	Welcome003       = &TwitchMessage{Command: "003", Message: "This server is rather new"}
	Welcome004       = &TwitchMessage{Command: "004", Message: "-"}
	Welcome375       = &TwitchMessage{Command: "375", Message: "-"}
	Welcome372       = &TwitchMessage{Command: "372", Message: "You are in a maze of twisty passages, all alike!"}
	Welcome376       = &TwitchMessage{Command: "376", Message: ">"}
	MembershipCap    = &TwitchMessage{Command: "CAP", Params: []string{"*", "ACK"}, Message: "twitch.tv/membership"}
	TagsCap          = &TwitchMessage{Command: "CAP", Params: []string{"*", "ACK"}, Message: "twitch.tv/tags"}
	CommandsCap      = &TwitchMessage{Command: "CAP", Params: []string{"*", "ACK"}, Message: "twitch.tv/commands"}
	Welcome          = []*TwitchMessage{Welcome001, Welcome002, Welcome003, Welcome004, Welcome375, Welcome372, Welcome376}
	ListeningMessage = &TwitchMessage{
		Command: "PRIVMSG",
		Prefix: &Prefix{
			Name: "blaaabot",
			User: "blaaabot",
			Host: "blaaabot"},
		Params:  []string{"#blaaabot"},
		Message: "Hello? Are you listening?"}

	ListeningEvent = messages.Event{
		Transport: transport.CHANNEL,
		Target:    ListeningMessage.Params[0][1:],
		Sender: &domain.User{
			Name: ListeningMessage.Prefix.User,
		},
		Message: ListeningMessage.Message,
	}

	Received = make(chan *TwitchMessage)
)

func joinResponse(channel string, loggedInUser string) []*TwitchMessage {
	return []*TwitchMessage{
		{Command: "JOIN", Params: []string{channel}},
		{Command: "353", Params: []string{loggedInUser, "=", channel}, Message: loggedInUser},
		{Command: "366", Params: []string{loggedInUser, channel}},
	}
}

func testServer(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.Upgrader{}
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade error:", err)
		return
	}
	defer func() {
		err := c.Close()
		if err != nil {
			fmt.Print("Error closing client", err)
		}
	}()

	var loggedIn string
	seen := make(map[string]bool)
	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			log.Print("read:", err)
			return
		}
		log.Printf("recv: %s", message)
		mssg := Parse(string(message))

		sendMultiple := func(c *websocket.Conn, mssgs []*TwitchMessage) {
			for _, mssg := range mssgs {
				err := c.WriteMessage(mt, mssg.Bytes())
				if err != nil {
					log.Print("writeerror:", err)
					return
				}
			}
		}

		// Handle input
		switch mssg.Command {
		case "PASS":
			seen["PASS"] = true
			if seen["NICK"] {
				sendMultiple(c, Welcome)
			}
		case "NICK":
			seen["NICK"] = true
			loggedIn = mssg.Params[0]
			if seen["PASS"] {
				sendMultiple(c, Welcome)
			}
		case "CAP":
			if mssg.Params[0] == "REQ" {
				switch mssg.Message {
				case "twitch.tv/membership":
					sendMultiple(c, []*TwitchMessage{MembershipCap})
				case "twitch.tv/tags":
					sendMultiple(c, []*TwitchMessage{TagsCap})
				case "twitch.tv/commands":
					sendMultiple(c, []*TwitchMessage{CommandsCap})
				}
			}
		case "JOIN":
			sendMultiple(c, joinResponse(mssg.Params[0], loggedIn))
		case "LISTENING":
			sendMultiple(c, []*TwitchMessage{ListeningMessage, ListeningMessage, ListeningMessage})
		case "PRIVMSG", "WHISPER":
			Received <- mssg
		}
	}
}

func startServer(done <-chan struct{}) {
	server := &http.Server{
		Addr:    ":5555",
		Handler: http.HandlerFunc(testServer),
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil {
			log.Print("received error from server ", err)
		}
	}()

	// Waits for done to close and stops the server then
	go func() {
		<-done
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		err := server.Shutdown(ctx)
		if err != nil {
			log.Fatal("error shutting down server ", err)
		}
	}()
}

func TestNew(t *testing.T) {
	done := make(chan struct{})
	startServer(done)
	defer func() {
		close(done)
		// Wait for server to shutdown
		<-time.NewTimer(500 * time.Millisecond).C
	}()

	testURL, _ := url.Parse("ws://localhost:5555")
	client, err := New(&config.ConnectionConfig{
		URL:      testURL,
		Username: "mo",
		Token:    "someToken",
		ChannelConfig: &config.ChannelConfig{
			JoinedChannels: []string{"mo"},
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if client == nil {
		t.Fatal("Returned nil client")
	}
	defer func() {
		err := client.Close()
		if err != nil {
			t.Log("Error closing client", err)
		}
	}()
}

func TestTwitchClient_Listen(t *testing.T) {
	done := make(chan struct{})
	startServer(done)
	defer func() {
		close(done)
		// Wait for server to shutdown
		<-time.NewTimer(500 * time.Millisecond).C
	}()

	testURL, _ := url.Parse("ws://localhost:5555")
	client, err := New(&config.ConnectionConfig{
		URL:      testURL,
		Username: "mo",
		Token:    "someToken",
		ChannelConfig: &config.ChannelConfig{
			JoinedChannels: []string{"mo"},
		},
	})
	if err != nil {
		t.Fatal("Failed to create client", err)
	}
	defer func() {
		err := client.Close()
		if err != nil {
			t.Log("Error closing client", err)
		}
	}()

	eventChannel := client.Listen()

	// Send sign that client is listening
	err = send(client.conn, &TwitchMessage{Command: "LISTENING"})
	if err != nil {
		log.Fatal("Failed to send 'LISTENING' message", err)
	}
	received := 0
	for received < 3 {
		select {
		case event := <-eventChannel:
			if event.Eq(&ListeningEvent) {
				received++
			} else {
				t.Fatalf("Received not supported event\n %v\n Should match:\n%v\n", event, &ListeningEvent)
			}
		case <-time.NewTimer(time.Second).C:
			t.Fatalf("TImed out waiting for %d Event", received+1)
		}
	}
}

func TestTwitchClient_StartSender(t *testing.T) {
	// Start test server
	done := make(chan struct{})
	startServer(done)
	defer func() {
		close(done)
		// Wait for server to shutdown
		<-time.NewTimer(500 * time.Millisecond).C
	}()

	// test events
	mssgs := []*messages.Message{
		{
			Code:      messagecode.Code(255),
			Transport: transport.CHANNEL,
			Target:    "blaaabot",
			Message:   "TestMessage",
		}, {
			Code:      messagecode.Code(255),
			Transport: transport.WHISPER,
			Target:    "blaaabot",
			Message:   "TestMessage 2",
		}, {
			Code:      messagecode.Code(255),
			Transport: transport.CHANNEL,
			Target:    "blaaabot",
			Message:   "TestMessage 3",
		},
	}
	twMessage := []*TwitchMessage{
		{
			Command: "PRIVMSG",
			Params: []string{
				fmt.Sprintf("#%s", mssgs[0].Target),
			},
			Message: mssgs[0].Message,
		}, {
			Command: "PRIVMSG",
			Params:  []string{"#mo"},
			Message: fmt.Sprintf("/w %s %s", mssgs[1].Target, mssgs[1].Message),
		}, {
			Command: "PRIVMSG",
			Params: []string{
				fmt.Sprintf("#%s", mssgs[2].Target),
			},
			Message: mssgs[2].Message,
		},
	}

	// Create client
	testURL, _ := url.Parse("ws://localhost:5555")
	client, err := New(&config.ConnectionConfig{
		URL:      testURL,
		Username: "mo",
		Token:    "someToken",
		ChannelConfig: &config.ChannelConfig{
			JoinedChannels: []string{"mo"},
		},
	})
	if err != nil {
		t.Fatal("Failed to create client", err)
	}
	defer func() {
		err := client.Close()
		if err != nil {
			t.Log("Error closing client", err)
		}
	}()

	// Start Server
	in := make(chan *messages.Message)
	defer close(in)
	go func() {
		client.StartSender(done, in)
	}()

	// Send events
	for _, mssg := range mssgs {
		in <- mssg
	}

	for i := 0; i < len(twMessage); i++ {
		select {
		case received := <-Received:
			if twMessage[i].Eq(received) {
				continue
			}
			// otherwise some unknown message has been sent
			t.Fatalf("Event out of order received.\nReceived: %s\nExpected: %s\n", received, twMessage[i])
		case <-time.NewTimer(time.Second).C:
			t.Fatal("Timed out before message has been sent")
		}
	}
}

func TestTwitchClient_StartSenderMessageLimitsWhisper(t *testing.T) {
	// Start test server
	done := make(chan struct{})
	startServer(done)
	defer func() {
		close(done)
		// Wait for server to shutdown
		<-time.NewTimer(500 * time.Millisecond).C
	}()

	// Generate message with large content
	var strBuilder strings.Builder
	for i := 0; i < 1000; i++ {
		strBuilder.WriteString("aa ")
	}

	// test events
	mssgs := []*messages.Message{
		{
			Code:      messagecode.Code(255),
			Transport: transport.WHISPER,
			Target:    "blaaabot",
			Message:   strBuilder.String(),
		},
	}

	// Create client
	testURL, _ := url.Parse("ws://localhost:5555")
	client, err := New(&config.ConnectionConfig{
		URL:      testURL,
		Username: "mo",
		Token:    "someToken",
		ChannelConfig: &config.ChannelConfig{
			JoinedChannels: []string{"mo"},
		},
	})
	if err != nil {
		t.Fatal("Failed to create client", err)
	}
	defer func() {
		err := client.Close()
		if err != nil {
			t.Log("Error closing client", err)
		}
	}()

	// Start Server
	in := make(chan *messages.Message)
	defer close(in)
	go func() {
		client.StartSender(done, in)
	}()

	// Send events
	for _, mssg := range mssgs {
		in <- mssg
	}

	absoluteSize := 0
OUTER:
	for i := 0; i < 7; i++ {
		select {
		case received := <-Received:
			mssg := strings.Replace(received.Message, "/w blaaabot ", "", 1)
			if len(mssg) > 500 {
				t.Fatal("Message length exceeds limit of 500", len(received.Message), received)
			}
			absoluteSize += len(mssg)
		case <-time.NewTimer(time.Second).C:
			if absoluteSize != 3000-7 {
				t.Fatalf("Sent messages don't equal in result size. Expected: %d, actual: %d", 3000, absoluteSize)
			} else {
				break OUTER
			}
		}
	}
}

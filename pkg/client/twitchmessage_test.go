package client

import (
	"strings"
	"testing"
)

func TestPrefix_String(t *testing.T) {
	prefix := &Prefix{
		Name: "name",
		User: "user",
		Host: "host",
	}

	if prefix.String() != ":name!user@host" {
		t.Fatal()
	}

	prefix = &Prefix{
		Name: "name",
		Host: "host",
	}

	if prefix.String() != ":name@host" {
		t.Fatal()
	}

	prefix = &Prefix{
		Name: "name",
	}

	if prefix.String() != ":name" {
		t.Fatal()
	}
}

func TestTwitchMessage_Eq(t *testing.T) {
	message := TwitchMessage{
		Tags: map[string]string{
			"hello": "world",
			"this":  "isSparta",
		},
		Prefix: &Prefix{
			Name: "name",
			User: "user",
			Host: "host",
		},
		Command: "COMMAND",
		Params: []string{
			"param1", "param2",
		},
		Message: "A Message can also contain whitespaces!",
	}
	message2 := message

	if !(&message).Eq(&message2) {
		t.Fatal()
	}

	tagChanged := message
	tagChanged.Tags = map[string]string{
		"baba": "isyou",
	}
	if (&message).Eq(&tagChanged) {
		t.Fatalf("Shouldn't equal:\n%s\n%s\n", &message, &tagChanged)
	}

	prefixChanged := message
	prefixChanged.Prefix = &Prefix{
		Name: "name2",
		User: "user2",
		Host: "user2",
	}
	if (&message).Eq(&prefixChanged) {
		t.Fatalf("Shouldn't equal:\n%s\n%s\n", &message, &prefixChanged)
	}

	commandChanged := message
	commandChanged.Command = "OTHER"
	if (&message).Eq(&commandChanged) {
		t.Fatalf("Shouldn't equal:\n%s\n%s\n", &message, &commandChanged)
	}

	paramsChanged := message
	paramsChanged.Params = []string{
		"p1", "p2",
	}
	if (&message).Eq(&paramsChanged) {
		t.Fatalf("Shouldn't equal:\n%s\n%s\n", &message, &paramsChanged)
	}

	messageChanged := message
	messageChanged.Message += " And can be appended!"
	if (&message).Eq(&messageChanged) {
		t.Fatalf("Shouldn't equal:\n%s\n%s\n", &message, &messageChanged)
	}
}

func TestTwitchMessage_String(t *testing.T) {
	message := &TwitchMessage{
		Command: "JOIN",
	}
	if message.String() != "JOIN" {
		t.Fatal()
	}
	message.Params = []string{
		"param", "=", "awesome",
	}
	if message.String() != "JOIN param = awesome" {
		t.Fatal()
	}
	message.Message = "Hello world!"
	if message.String() != "JOIN param = awesome :Hello world!" {
		t.Fatal()
	}
	message.Prefix = &Prefix{
		Name: "name",
	}
	if message.String() != ":name JOIN param = awesome :Hello world!" {
		t.Fatal()
	}
	message.Prefix.Host = "host"
	if message.String() != ":name@host JOIN param = awesome :Hello world!" {
		t.Fatal()
	}
	message.Prefix.User = "user"
	if message.String() != ":name!user@host JOIN param = awesome :Hello world!" {
		t.Fatal()
	}
	message.Tags = map[string]string{
		"tag1": "val1",
		"tag2": "val2",
	}
	ser := message.String()
	if !strings.HasPrefix(ser, "@tag1=val1;tag2=val2") && !strings.HasPrefix(ser, "@tag2=val2;tag1=val1") {
		t.Fatalf("'%s'\nShould be prefixed with:\n%s\n%s\n", message.String(), "@tag1=val1;tag2=val2", "@tag2=val2;tag1=val1")
	}
	message.Tags = nil
	bites := string(message.Bytes())
	// Tags are excluded, as their order is not guaranteed
	if message.String() != bites {
		t.Fatalf("String should equal byte representation:\n%s\n%s\n", ser, bites)
	}
}

package client

import "testing"

func TestParseTags(t *testing.T) {
	if m := ParseTags("@thisisinvalid;asfasfs"); len(m) != 0 {
		t.Fatalf("Should ignore invalid pairs")
		return
	}

	if m := ParseTags("@thisisinvalid;asfasfs;valid=pair"); len(m) != 1 {
		t.Fatalf("Should extract valid key-value pairs")
		return
	}

	if m := ParseTags("these=are;valid=pairs"); len(m) != 2 {
		t.Fatalf("Should work without prefixed by '@'")
		return
	}
}

func TestParsePrefix(t *testing.T) {
	if prefix := ParsePrefix(":servername"); prefix.Name != "servername" || prefix.Host != "" || prefix.User != "" {
		t.Fatal("Should parse servername only", prefix)
		return
	}

	if prefix := ParsePrefix(":servername!username@hostname"); prefix.Name != "servername" || prefix.Host != "hostname" || prefix.User != "username" {
		t.Fatal("Should parse name, user and host", prefix)
		return
	}

	if prefix := ParsePrefix(":servername@hostname"); prefix.Name != "servername" || prefix.Host != "hostname" {
		t.Fatal("Should parse name and host", prefix)
		return
	}

	if prefix := ParsePrefix(":servername!username"); prefix.Name != "servername" || prefix.User != "" {
		t.Fatal("Should ignore user if specified without host", prefix)
		return
	}

	if prefix := ParsePrefix("servername!username@hostname"); prefix.Name != "servername" || prefix.Host != "hostname" || prefix.User != "username" {
		t.Fatal("Should also parse without prefixed with ':'", prefix)
		return
	}
}

func TestParseParams(t *testing.T) {
	params, trailing := ParseParams(" hello these are params :and this is a message!")
	if params == nil || trailing == "" {
		t.Fatal("Should extract trailing and params", params, trailing)
		return
	}

	if params[0] != "hello" || params[1] != "these" || params[2] != "are" || params[3] != "params" {
		t.Fatalf("Failed to extract params. Expected: [\"hello\", \"these\", \"are\", \"params\"], actual: %s", params)
		return
	}

	if trailing != "and this is a message!" {
		t.Fatal("Badly extracted trailing message", trailing)
		return
	}

	params, trailing = ParseParams(" :this trailing contains ' :' a second time")
	if len(params) != 0 {
		t.Fatal("Badly extracted params", len(params), params)
		return
	}

	if trailing != "this trailing contains ' :' a second time" {
		t.Fatal("Should ignore sexond ' :' in message", params, trailing)
		return
	}
}

func TestParse(t *testing.T) {
	mssg := Parse("@tag1=value1;tag2=value2 :nickname!user@hostname COMMAND param1 param2 :Message!")
	if mssg == nil {
		t.Fatal("Returned nil on parsing")
		return
	}

	if len(mssg.Tags) != 2 || mssg.Tags["tag1"] != "value1" || mssg.Tags["tag2"] != "value2" {
		t.Fatal("Badly parsed tags", mssg)
		return
	}

	if mssg.Prefix.Name != "nickname" || mssg.Prefix.User != "user" || mssg.Prefix.Host != "hostname" {
		t.Fatal("Badly parsed prefix", mssg)
		return
	}

	if mssg.Command != "COMMAND" {
		t.Fatal("Badly parsed command", mssg)
		return
	}

	if len(mssg.Params) != 2 || mssg.Params[0] != "param1" || mssg.Params[1] != "param2" {
		t.Fatal("Badly parsed params", mssg)
		return
	}

	if mssg.Message != "Message!" {
		t.Fatal("Badly parsed message", mssg)
		return
	}
}

func TestParse_WithoutTags(t *testing.T) {
	mssg := Parse(":nickname!user@hostname COMMAND param1 param2 :Message!")
	if mssg == nil {
		t.Fatal("Returned nil on parsing")
		return
	}

	if mssg.Prefix.Name != "nickname" || mssg.Prefix.User != "user" || mssg.Prefix.Host != "hostname" {
		t.Fatal("Badly parsed prefix", mssg)
		return
	}

	if mssg.Command != "COMMAND" {
		t.Fatal("Badly parsed command", mssg)
		return
	}

	if len(mssg.Params) != 2 || mssg.Params[0] != "param1" || mssg.Params[1] != "param2" {
		t.Fatal("Badly parsed params", mssg)
		return
	}

	if mssg.Message != "Message!" {
		t.Fatal("Badly parsed message", mssg)
		return
	}
}

func TestParse_WithNickname(t *testing.T) {
	mssg := Parse(":nickname COMMAND :Message!")
	if mssg == nil {
		t.Fatal("Returned nil on parsing")
		return
	}

	if mssg.Prefix.Name != "nickname" {
		t.Fatal("Badly parsed prefix", mssg)
		return
	}

	if mssg.Command != "COMMAND" {
		t.Fatal("Badly parsed command", mssg)
		return
	}

	if mssg.Message != "Message!" {
		t.Fatal("Badly parsed message", mssg)
		return
	}
}

func TestParse_WithParamsAndMessage(t *testing.T) {
	mssg := Parse("COMMAND param1 param2 :Message!")
	if mssg == nil {
		t.Fatal("Returned nil on parsing")
		return
	}

	if mssg.Command != "COMMAND" {
		t.Fatal("Badly parsed command", mssg)
		return
	}

	if len(mssg.Params) != 2 || mssg.Params[0] != "param1" || mssg.Params[1] != "param2" {
		t.Fatal("Badly parsed params", mssg)
		return
	}

	if mssg.Message != "Message!" {
		t.Fatal("Badly parsed message", mssg)
		return
	}
}

func TestParse_WithParams(t *testing.T) {
	mssg := Parse("COMMAND param1 param2")
	if mssg == nil {
		t.Fatal("Returned nil on parsing")
		return
	}

	if mssg.Command != "COMMAND" {
		t.Fatal("Badly parsed command", mssg)
		return
	}

	if len(mssg.Params) != 2 || mssg.Params[0] != "param1" || mssg.Params[1] != "param2" {
		t.Fatal("Badly parsed params", mssg)
		return
	}
}

func TestParse_CommandOnly(t *testing.T) {
	mssg := Parse("COMMAND")
	if mssg == nil {
		t.Fatal("Returned nil on parsing")
		return
	}

	if mssg.Command != "COMMAND" {
		t.Fatal("Badly parsed command", mssg)
		return
	}
}

package client

import (
	"bytes"
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"strings"
)

// Prefix represents the IRCv3 Prefix of a IRC-Message.
type Prefix struct {
	// Name is either the Servername or the nickname of the sender
	Name string
	// User represents the optional user the message is from.
	User string
	// Host represents the server host address of the sender.
	Host string
}

func (prefix *Prefix) String() (result string) {
	if prefix.User != "" && prefix.Host != "" {
		result = fmt.Sprintf(":%s!%s@%s", prefix.Name, prefix.User, prefix.Host)
	} else if prefix.Host != "" {
		result = fmt.Sprintf(":%s@%s", prefix.Name, prefix.Host)
	} else {
		result = fmt.Sprintf(":%s", prefix.Name)
	}
	return
}

// TwitchMessage is the message in IRCv3 format sent from or to Twitch.
type TwitchMessage struct {
	// Tags containing meta-information of the message.
	Tags map[string]string
	// Prefix contains information about the sender of the message.
	*Prefix
	// Command the message represents.
	Command string
	// Params of the command.
	Params []string
	// Message represents the trailing parameter which Twitch generally handles as Message field.
	Message string
}

// Eq checks equality of fields present in the target TwitchMessage  with the ones of
// param `to`.
func (mssg *TwitchMessage) Eq(to *TwitchMessage) (out bool) {
	out = mssg.Command == to.Command
	if mssg.Message != "" {
		out = out && mssg.Message == to.Message
	}
	if mssg.Prefix != nil {
		if mssg.Prefix.Host != "" {
			out = out && mssg.Prefix.Host == to.Prefix.Host
		}
		if mssg.Prefix.Name != "" {
			out = out && mssg.Prefix.Name == to.Prefix.Name
		}
		if mssg.Prefix.User != "" {
			out = out && mssg.Prefix.User == to.Prefix.User
		}
	}
	if mssg.Params != nil {
		for i, param := range mssg.Params {
			out = out && param == to.Params[i]
		}
	}
	if mssg.Tags != nil {
		for key, value := range mssg.Tags {
			out = out && to.Tags[key] == value
		}
	}
	return
}

func (mssg *TwitchMessage) buffer() *bytes.Buffer {
	// Buffer for whole message
	messageBuf := new(bytes.Buffer)
	// Tags Buffer
	if mssg.Tags != nil {
		tBuffer := new(bytes.Buffer)
		for key, value := range mssg.Tags {
			_, err := fmt.Fprintf(tBuffer, "%s=%s;", key, value)
			if err != nil {
				elog.Fatal(fmt.Errorf("failed to serialize Tags of TwitchMessage: %w", err))
			}
		}
		strTags := tBuffer.String()
		if strTags != "" {
			_, err := fmt.Fprintf(messageBuf, "@%s ", strTags[:len(strTags)-1])
			if err != nil {
				elog.Fatal(fmt.Errorf("failed to serialize TwitchMessage: %w", err))
			}
		}
	}

	if mssg.Prefix != nil {
		_, err := fmt.Fprintf(messageBuf, "%s ", mssg.Prefix.String())
		if err != nil {
			elog.Fatal(fmt.Errorf("failed to serialize Prefix of TwitchMessage: %w", err))
		}
	}

	_, err := fmt.Fprintf(messageBuf, "%s", mssg.Command)
	if err != nil {
		elog.Fatal(fmt.Errorf("failed to serialize TwitchMessage command: %w", err))
	}

	if mssg.Params != nil {
		_, err = fmt.Fprintf(messageBuf, " %s", strings.Join(mssg.Params, " "))
		if err != nil {
			elog.Fatal(fmt.Errorf("failed to serialize Params of TwitchMessage: %w", err))
		}
	}

	if mssg.Message != "" {
		_, err := fmt.Fprintf(messageBuf, " :%s", mssg.Message)
		if err != nil {
			elog.Fatal(fmt.Errorf("failed to serialize Message of TwitchMessage: %w", err))
		}
	}

	return messageBuf
}

func (mssg *TwitchMessage) String() string {
	return mssg.buffer().String()
}

// Bytes returns the Bytes-Serialization of the TwitchMessage.
func (mssg *TwitchMessage) Bytes() []byte {
	return mssg.buffer().Bytes()
}

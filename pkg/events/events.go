package events

import (
	"encoding/json"
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
)

// EventID represents identifiers
type EventID string

// EventType is used to differentiate between different types of BotEvents.
type EventType string

const (
	// EventIDGetConfig represents the ID of the ControlCommand to get the current configuration.
	EventIDGetConfig EventID = "GETCONFIG"
	// EventIDSetConfig represents the ID of a ControlCommand to set the configuration of the bot.
	EventIDSetConfig EventID = "SETCONFIG"
	// EventIDStart represents the ID of a ControlCommand to start the bot.
	EventIDStart EventID = "START"
	// EventIDStop represents the ID of a ControlCommand to start the bot.
	EventIDStop EventID = "STOP"
	// EventIDBotInfo represents the ID of a event from the bot to the ui containing status information.
	EventIDBotInfo EventID = "BOTSTATUS"

	// EventIDConnectionSuccess identifies an event to signal a successful connection establishment.
	EventIDConnectionSuccess EventID = "CONNECTED"
	// EventIDCommandStarted identifies an event to signal a successful start of a command.
	EventIDCommandStarted EventID = "STARTED COMMAND"
	// EventIDCommandFinished identifies an event to signal a successful finish of a command.
	EventIDCommandFinished EventID = "FINISHED COMMAND"
	// EventIDCommandNotSupported identifies an event to signal that a command is not supported.
	EventIDCommandNotSupported EventID = "NOT SUPPORTED"
	// EventIDCommandCooldown identifies an event to signal that a command still has a cooldown.
	EventIDCommandCooldown EventID = "COMMAND COOLDOWN"
	// EventIDNotPermitted identifies an event to signal that a user is not permitted to issue a command.
	EventIDNotPermitted EventID = "NOT PERMITTED"

	// EventIDBotStarted identifies an event to signal that the bot started successfully.
	EventIDBotStarted EventID = "STARTED BOT"
	// EventIDBotStopped identifies an event to signal that the bot stopped successfully.
	EventIDBotStopped EventID = "STOPPED BOT"
	// EventIDBotFinalizing identifies an event to signal that the bot started finishing its execution.
	EventIDBotFinalizing EventID = "FINALIZING BOT"

	// EventIDError identifies a LogEvent as error event.
	EventIDError EventID = "ERROR"
	// EventIDDebug identifies a LogEvent as debug event.
	EventIDDebug EventID = "DEBUG"
	// EventIDInfo identifies a LogEvent as info event.
	EventIDInfo EventID = "INFO"
	// EventIDWarn identifies a LogEvent as warning event.
	EventIDWarn EventID = "WARNING"

	// EventIDChatMessage identifies a LogEvent containing a message sent to a chat.
	EventIDChatMessage EventID = "CHAT MESSAGE"

	// LogEventType is used to identify LogEvents.
	LogEventType EventType = "LOG EVENT"
	// CommandResponseType is the type of a event as response to a command.
	CommandResponseType EventType = "COMMAND RESPONSE"
	// CommandType is the type of a event to trigger a bot command.
	CommandType EventType = "COMMAND"
)

// A BotEvent is sent to and from the bot to trigger commands, responses to commands and others.
type BotEvent struct {
	Type EventType
	ID   EventID
	Body interface{} `json:",omitempty"`
}

// CommandResponse structures the responses over the websocket connection.
type CommandResponse struct {
	BotEvent
	// The Code is used to summarize the response like HTTP Response codes.
	Code int
}

func configUnmarshaller(b []byte) (interface{}, error) {
	var cfg config.Config
	err := json.Unmarshal(b, &cfg)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling config: %v", err)
	}
	return &cfg, nil
}

var unmarshallers = map[EventID]func([]byte) (interface{}, error){
	EventIDGetConfig: configUnmarshaller,
	EventIDSetConfig: configUnmarshaller,
}

// AddBodyUnmarshaller adds an unmarshaler for BotEvent bodys to use when unmarshalling
// a BotEvent through the `json` library.
func AddBodyUnmarshaller(id EventID, unmarshaller func([]byte) (interface{}, error)) {
	unmarshallers[id] = unmarshaller
}

// UnmarshalJSON converts a serialized BotEvent to its golang structure using the
// Body Unmarshaller registered through `AddBodyUnmarshaller` and the default Config
// Unmarshaller
func (event *BotEvent) UnmarshalJSON(data []byte) error {
	var unmarshalled struct {
		Type EventType
		ID   EventID
		Body json.RawMessage
	}

	err := json.Unmarshal(data, &unmarshalled)
	if err != nil {
		return fmt.Errorf("error unmarshalling ControlCommand: %v", err)
	}
	event.Type = unmarshalled.Type
	event.ID = unmarshalled.ID
	if unmarshaler, ok := unmarshallers[event.ID]; ok && unmarshalled.Body != nil {
		// Unmarshal raw body
		body, err := unmarshaler(unmarshalled.Body)
		if err != nil {
			return fmt.Errorf("error unmarshalling ControlCommand with unmarshaler for '%s': %v", event.ID, err)
		}
		event.Body = body
	} else {
		var body interface{}
		err = json.Unmarshal(unmarshalled.Body, &body)
		event.Body = body
	}
	return nil
}

package events

import (
	"encoding/json"
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"testing"
)

func TestUnmarshalBotEvent(t *testing.T) {
	event := &BotEvent{
		Type: "TestType",
		ID:   "TestID",
		Body: &config.Config{
			ConnectionConfig: &config.ConnectionConfig{},
			MessagingConfig:  &config.MessagingConfig{},
			CommandConfig:    &config.CommandConfig{},
			AuthConfig:       &config.AuthConfig{},
		},
	}

	ser, err := json.MarshalIndent(event, "", " ")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(string(ser))

	type Alias BotEvent
	var deser *struct {
		Body *config.Config
		*Alias
	}
	err = json.Unmarshal(ser, &deser)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Printf("%+v\n", deser)
}

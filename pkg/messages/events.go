package messages

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain"
)

// Event represents an event sent from Twitch to the Bot.
type Event struct {
	Transport transport.Transport
	Target    string
	Sender    *domain.User
	Message   string
}

func (event *Event) String() string {
	return fmt.Sprintf("{Transport: %v, Target: %s, Sender: %s, Message: %s}", event.Transport, event.Target, event.Sender, event.Message)
}

// Eq compares an event with an interface and returns their equality
func (event *Event) Eq(o interface{}) bool {
	if ev, ok := o.(*Event); ok {
		return event.Transport == ev.Transport &&
			event.Message == ev.Message &&
			event.Target == ev.Target &&
			event.Sender.Eq(ev.Sender)
	}
	return false
}

// ResponseTarget calculates the `Target` attribute of an Answer-Event
// to the given event.
func (event *Event) ResponseTarget() string {
	result := event.Target
	if event.Transport == transport.WHISPER {
		result = event.Sender.Name
	}
	return result
}

// BundledEvent represents events grouped by Transport, Target and Command.
type BundledEvent struct {
	Transport transport.Transport
	Target    string
	RawEvents []*Event
	Command   string
}

// Eq compares equality by comparing all fields and all raw events.
func (event *BundledEvent) Eq(o interface{}) bool {
	if b, ok := o.(*BundledEvent); ok {
		if event.Transport != b.Transport ||
			event.Target != b.Target ||
			event.Command != b.Command {
			return false
		}
		if len(b.RawEvents) != len(event.RawEvents) {
			return false
		}
		for i, v := range event.RawEvents {
			if v != b.RawEvents[i] {
				return false
			}
		}
		return true
	}
	return false
}

func (event *BundledEvent) String() string {
	return fmt.Sprintf("{Transport: %v, Target: %s, Command: %s, RawEvents: %v}", event.Transport, event.Target, event.Command, event.RawEvents)
}

// Senders returns an array of all users which sent events contained in this bundled event.
func (event *BundledEvent) Senders() []*domain.User {
	var senders []*domain.User
	alreadyPresent := make(map[domain.ID]struct{})
	for _, event := range event.RawEvents {
		if event == nil || event.Sender == nil {
			continue
		}
		if _, ok := alreadyPresent[event.Sender.ID]; !ok {
			alreadyPresent[event.Sender.ID] = struct{}{}
			senders = append(senders, event.Sender)
		}
	}
	return senders
}

package messages

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
)

// Message is the dto representing messages sent from this bot to Twitch.
type Message struct {
	Code      messagecode.Code
	Transport transport.Transport
	Message   string
	Target    string
}

func (m *Message) String() string {
	return fmt.Sprintf("Message(code=%v, transport=%v, target=%s, message=%s)", m.Code, m.Transport, m.Target, m.Message)
}

// Eq compares some other instance with a message and returns if their fields match.
func (m *Message) Eq(o interface{}) bool {
	if m2, ok := o.(*Message); ok {
		equal := true
		if m.Code != 0 {
			equal = equal && m.Code == m2.Code
		}
		if m.Message != "" {
			equal = equal && m.Message == m2.Message
		}
		if m.Transport != 0 {
			equal = equal && m.Transport == m2.Transport
		}
		if m.Target != "" {
			equal = equal && m.Target == m2.Target
		}
		return equal
	}
	return false
}

// NewChannelAnswer creates a new answer-Message to a given event with the given code and mssg.
func NewChannelAnswer(event *BundledEvent, code messagecode.Code, mssg string) *Message {
	return &Message{
		Code:      code,
		Transport: event.Transport,
		Target:    event.Target,
		Message:   mssg,
	}
}

// NewWhisperAnswer creates new messages for the given events by applying the given function to map
// events to (target, message) pairs with the given code.
func NewWhisperAnswer(events []*Event, code messagecode.Code, mssgGen func(event *Event) (string, string)) []*Message {
	var mssgs []*Message
	for _, event := range events {
		target, mssg := mssgGen(event)
		mssgs = append(mssgs, &Message{
			Code:      code,
			Transport: event.Transport,
			Target:    target,
			Message:   mssg,
		})
	}
	return mssgs
}

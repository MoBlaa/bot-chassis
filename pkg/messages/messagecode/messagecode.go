package messagecode

// A Code contains information of the Type of the message.
type Code uint64

const (
	// Additional Message-Code for the Buh-Command

	// Buh represents the return message of a buh-command.
	Buh Code = 69
	// Test represents the return message of a test-command.
	Test Code = 42

	// Message Codes for duels are in the inclusive range of 0-99

	// 0 is the default value for integers and used for not given message codes

	// CommandCooldown represents a message returned to a Command with a participant still in cooldown.
	CommandCooldown Code = 303
)

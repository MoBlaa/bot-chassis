package commands

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
)

// Buh handles a bundled event by returning a simple scared face "D:" message.
func Buh(event *messages.BundledEvent) <-chan *messages.Message {
	out := make(chan *messages.Message)
	go func() {
		defer close(out)
		out <- &messages.Message{
			Code:      messagecode.Buh,
			Transport: event.Transport,
			Target:    event.Target,
			Message:   "D:",
		}
	}()
	return out
}

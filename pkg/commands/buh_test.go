package commands

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
	"testing"
	"time"
)

func TestBuh(t *testing.T) {
	event := &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "blaaabot",
		RawEvents: []*messages.Event{},
		Command:   "!buh",
	}

	output := Buh(event)

	select {
	case <-time.NewTimer(10 * time.Millisecond).C:
		t.Fatal("Timed out waiting for scared message!")
	case mssg := <-output:
		if mssg == nil || mssg.Code != messagecode.Buh {
			t.Fatal("Response is not a Buh-Message!", mssg)
		}
	}

	if _, opened := <-output; opened {
		t.Fatal("Should close after returning a single Buh-Message!")
	}
}

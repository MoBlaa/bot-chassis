package commands

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
)

// Test handles a bundled event by returning a simple "Success!"
func Test(event *messages.BundledEvent) <-chan *messages.Message {
	out := make(chan *messages.Message)
	go func() {
		defer close(out)
		out <- &messages.Message{
			Code:      messagecode.Test,
			Transport: event.Transport,
			Target:    event.Target,
			Message:   "Success!",
		}
	}()
	return out
}

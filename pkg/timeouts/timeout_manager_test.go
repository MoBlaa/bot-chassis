package timeouts

import (
	"testing"
	"time"
)

func TestCooldownManager_Add(t *testing.T) {
	mgr := New()
	handlerTriggered := make(chan bool)
	defer close(handlerTriggered)
	defer mgr.Close()

	event := "event"
	handler := func(event interface{}) {
		handlerTriggered <- true
	}

	// Test triggering a cooldown
	err := mgr.Add(event, handler, 5*time.Millisecond)
	if err != nil {
		t.Fatal("failed to add cooldown handler", err)
	}
	if contained := mgr.Has(event); !contained {
		t.Fatal("cooldown hasn't been added")
	}
	select {
	case _, more := <-handlerTriggered:
		if !more {
			t.Fatal("closed handler early")
		}
	case <-time.NewTimer(time.Second).C:
	}
}

func TestCooldownManager_Remove(t *testing.T) {
	mgr := New()
	handlerTriggered := make(chan bool)
	defer close(handlerTriggered)
	defer mgr.Close()

	event := "event"
	handler := func(event interface{}) {
		handlerTriggered <- true
	}
	// Test early abortion
	err := mgr.Add(event, handler, time.Second)
	if err != nil {
		t.Fatal("failed add cooldown handler", err)
	}
	mgr.Remove(event)
	if contained := mgr.Has(event); contained {
		t.Fatal("cooldown hasn't been removed")
	}
}

func TestCooldownManager_Close(t *testing.T) {
	mgr := New()
	handlerTriggered := make(chan bool)
	defer close(handlerTriggered)

	event := "event"
	handler := func(event interface{}) {
		handlerTriggered <- true
	}
	// Test closing a cooldown
	err := mgr.Add(event, handler, time.Second)
	if err != nil {
		t.Fatal("failed add cooldown handler", err)
	}
	mgr.Close()
	// Wait a bit
	<-time.NewTimer(50 * time.Millisecond).C
	if contained := mgr.Has(event); contained {
		t.Fatal("not properly closed/removed cooldown")
	}
}

func TestTom_GetRemaining(t *testing.T) {
	mgr := New()
	defer mgr.Close()

	key := "key"

	err := mgr.Add(key, mgr.Remove, time.Second)
	if err != nil {
		t.Fatal("Error while adding cooldown", err)
		return
	}

	remaining := mgr.GetRemaining(key)
	if remaining > time.Second {
		t.Fatal("Remaining should not be larger than timeout given to add!", remaining)
	}
}

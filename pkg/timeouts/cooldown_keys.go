package timeouts

// UserCommandKey represents the Key used to identify a cooldown for a user for the given command.
type UserCommandKey struct {
	User    string
	Command string
}

package timeouts

import (
	"errors"
	"sync"
	"time"
)

// The TimeoutManager is used to set, get and reset timeouts for command cooldowns.
type TimeoutManager interface {
	Add(source interface{}, handler func(duel interface{}), duration time.Duration) error
	Has(source interface{}) bool
	GetRemaining(source interface{}) time.Duration
	Remove(source interface{})
	Close()
}

type timeout struct {
	// resolved is the Channel getting closed if the timeout is resolved and not removed
	resolved chan struct{}
	// started marks the time the timeout has been created
	started time.Time
	// amount = the duration of the timeout
	amount time.Duration
}

type tom struct {
	// mux is used to manage concurrent access to a TimeoutManager
	mux sync.RWMutex
	// duelLocks is used to collect the mutexes handling timeouts for duels
	locks map[interface{}]*timeout
}

// New TimeoutManager Creation.
func New() TimeoutManager {
	return &tom{
		locks: make(map[interface{}]*timeout),
	}
}

// Add one duel with handler and timeout duration
func (mgr *tom) Add(duel interface{}, cooldownHandler func(duel interface{}), duration time.Duration) error {
	// Wait for access to TimeoutManager allowed
	mgr.mux.RLock()
	_, ok := mgr.locks[duel]
	if ok {
		mgr.mux.RUnlock()
		return errors.New("tried to create multiple cooldowns for one event")
	}
	mgr.mux.RUnlock()
	// Create new lock and goroutine handling the timeout
	done := make(chan struct{})
	// Goroutine listening on early abortion and timeout
	go func() {
		more := false
		select {
		case <-time.NewTimer(duration).C:
			// Handle Cooldown
			cooldownHandler(duel)
		case _, more = <-done:
			// Early abortion -> not triggering cooldown handler
		}
		// Finishing up
		mgr.mux.Lock()
		if more {
			close(done)
		}
		delete(mgr.locks, duel)
		mgr.mux.Unlock()
	}()
	// Wait for write access which means no one else is adding cooldowns
	mgr.mux.Lock()
	mgr.locks[duel] = &timeout{
		resolved: done,
		started:  time.Now(),
		amount:   duration,
	}
	mgr.mux.Unlock()
	return nil
}

// Has returns if a timeout exists for the given key.
func (mgr *tom) Has(key interface{}) (has bool) {
	mgr.mux.RLock()
	_, has = mgr.locks[key]
	mgr.mux.RUnlock()
	return
}

func (mgr *tom) GetRemaining(source interface{}) time.Duration {
	mgr.mux.RLock()
	to, exists := mgr.locks[source]
	mgr.mux.RUnlock()
	if !exists {
		return -1
	}

	remaining := to.amount - time.Now().Sub(to.started)
	if remaining > to.amount {
		remaining = 0
	}

	return remaining
}

// Remove a single duel.
func (mgr *tom) Remove(duel interface{}) {
	mgr.mux.RLock()
	if to, ok := mgr.locks[duel]; !ok {
		mgr.mux.RUnlock()
		return
	} else if to == nil {
		delete(mgr.locks, duel)
		mgr.mux.RUnlock()
		return
	}
	mgr.mux.RUnlock()
	mgr.mux.Lock()
	close(mgr.locks[duel].resolved)
	delete(mgr.locks, duel)
	mgr.mux.Unlock()
}

// Close the manager and early abort all cooldowns.
func (mgr *tom) Close() {
	mgr.mux.Lock()
	for key, done := range mgr.locks {
		close(done.resolved)
		delete(mgr.locks, key)
	}
	mgr.mux.Unlock()
}

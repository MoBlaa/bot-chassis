package pkg

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"sync"
)

type connection struct {
	id   string
	lock sync.Mutex
	conn *websocket.Conn
}

func (c *connection) WriteJSON(data interface{}) error {
	// Convert to json
	marshalled, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("failed to marshal response: %v", err)
	}
	elog.Debug(fmt.Sprintf("send: %s\n", string(marshalled)))

	// Write
	_, err = c.Write(marshalled)
	return err
}

func (c *connection) Write(data []byte) (n int, err error) {
	c.lock.Lock()
	err = c.conn.WriteMessage(websocket.TextMessage, data)
	if err == nil {
		n = len(data)
	} else {
		n = 0
	}
	c.lock.Unlock()
	return
}

func (c *connection) read() (mssgType int, data []byte, err error) {
	return c.conn.ReadMessage()
}

func (c *connection) close() (err error) {
	c.lock.Lock()
	err = c.conn.Close()
	c.lock.Unlock()
	return
}

package roles

// UserRole describes the role of the user in a channel.
type UserRole string

const (
	// Streamer is representing the role of a broadcaster.
	Streamer UserRole = "broadcaster"
	// Moderator is representing the role of a moderator.
	Moderator UserRole = "mod"
	// Subscriber is representing the role of someone which subscribed the channel.
	Subscriber UserRole = "subscriber"
)

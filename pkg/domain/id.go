package domain

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"hash/fnv"
	"os"
	"strconv"
)

// ID is used to uniquely identify an instance.
type ID uint64

func (id ID) String() string {
	return strconv.FormatUint(uint64(id), 10)
}

// NewID create a new hashed ID from given input.
func NewID(input ...string) ID {
	h := fnv.New32a()
	for _, str := range input {
		if _, err := h.Write([]byte(str)); err != nil {
			elog.Fatal(fmt.Errorf("error creating hash: %w", err))
		}
	}
	return ID(h.Sum32())
}

// ParseID reads a numeric string with base 10 and converts it as ID.
func ParseID(input string) ID {
	i, err := strconv.ParseUint(input, 10, 64)
	if err != nil {
		elog.Fatal(fmt.Errorf("failed to parse uint: %w", err))
		os.Exit(-1)
	}
	return ID(i)
}

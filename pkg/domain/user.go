package domain

import "gitlab.com/MoBlaa/bot-chassis/pkg/domain/roles"

// User represents an Twitch-Account.
type User struct {
	// ID of the User
	ID ID
	// Name to use for messages instead of the id
	Name string
	// Role of the user in the channel a message was sent
	Roles []roles.UserRole
}

// NewUser creates a new User by parsing the given id and with the given name.
func NewUser(id, name string, role ...roles.UserRole) *User {
	return &User{
		ID:    ParseID(id),
		Name:  name,
		Roles: role,
	}
}

// Eq compares two instances and returns true if *o* has type __*User__ and same id.
func (u *User) Eq(o interface{}) bool {
	if user, ok := o.(*User); ok {
		return user.ID == u.ID || user.Name == u.Name
	}
	return false
}

package auth

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/util"
	"strings"
)

// BlockSeparator is used to separate 4 character blocks in the authentication code.
const BlockSeparator = "-"

// Authentication holds the authentication information for user interfaces of the bot.
type Authentication struct {
	Token string
}

// New creates a new Authentication with a randomly generated token.
func New() *Authentication {
	return &Authentication{
		Token: util.RandomString(12),
	}
}

func (auth *Authentication) String() string {
	builder := strings.Builder{}
	for i, r := range auth.Token {
		if i != 0 && i%4 == 0 {
			builder.WriteString(BlockSeparator)
		}
		builder.WriteRune(r)
	}
	return builder.String()
}

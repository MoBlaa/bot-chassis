package elog

import "gitlab.com/MoBlaa/bot-chassis/pkg/events"

// Level the log will be shown at.
type Level string

const (
	// INFO level represents the informational level of logs.
	INFO Level = "INFO"
	// DEBUG level represents the level of logs used for debugging.
	DEBUG Level = "DEBUG"
	// ERROR level represents the level of logs to show errors.
	ERROR Level = "ERROR"
	// WARNING level represents the level at which warnings should be shown.
	WARNING Level = "WARNING"
)

// LogEvent represents a BotEvent with log level used to log to different targets.
type LogEvent struct {
	events.BotEvent
	Level Level
}

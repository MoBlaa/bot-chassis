package elog

import (
	"encoding/json"
	"gitlab.com/MoBlaa/bot-chassis/pkg/events"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"
)

// LogEventListener handle an instance of class LogEvent every time the elog is triggered.
type LogEventListener func(event interface{})

type logger struct {
	input        chan interface{}
	lastMessages []interface{}
	bufferSize   int

	listeners []LogEventListener
	lock      sync.Mutex

	fileout chan interface{}
}

// TODO: Make configurable
var logg = create(100)

func create(bufferSize int) *logger {
	logger := &logger{
		bufferSize: bufferSize,
		input:      make(chan interface{}, bufferSize),
	}
	go func() {
		for in := range logger.input {
			logger.lock.Lock()
			logger.fileout <- in
			logger.lastMessages = append(logger.lastMessages, in)
			for _, listener := range logger.listeners {
				listener(in)
			}
			length := len(logger.lastMessages)
			if length > logger.bufferSize {
				logger.lastMessages = logger.lastMessages[1:]
			}
			logger.lock.Unlock()
		}
	}()

	// Add console logger
	logger.addListener(func(event interface{}) {
		if l, ok := event.(*LogEvent); ok {
			log.Printf("%s %s\n", l.Level, l.Body)
		} else {
			log.Printf("%+v\n", l)
		}
	})

	// Add file log
	logger.fileout = make(chan interface{})
	err := os.MkdirAll(filepath.FromSlash("logs"), os.ModePerm)
	if err != nil {
		log.Fatal("failed to create logs directory", err)
	}
	file, err := os.OpenFile(filepath.FromSlash("logs/log."+time.Now().Format("2006_01_02_150405_Z0700")+".log"), os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Fatalf("failed to open logfile: %v", err)
	}
	// Write to file in separate goroutine
	go func() {
		defer func() {
			err := file.Close()
			if err != nil {
				log.Print(err)
			}
		}()
		for event := range logger.fileout {
			ser, err := json.Marshal(event)
			if err != nil {
				log.Fatal("failed to parse event to json", err)
			}
			_, err = file.WriteString(string(ser) + "\n")
			if err != nil {
				log.Fatal("failed to write log to file", err)
			}
		}
	}()

	return logger
}

// Info adds an event to the event logg with level `INFO`.
func Info(mssg string) {
	Log(INFO, events.EventIDInfo, mssg)
}

// Debug adds an event to the event logg with level `DEBUG`.
func Debug(mssg string) {
	Log(DEBUG, events.EventIDDebug, mssg)
}

// Error adds an event to the event logg with level `ERROR`.
func Error(err error) {
	Log(ERROR, events.EventIDError, err.Error())
}

// Warn adds an event to the event logg with level `WARNING`.
func Warn(mssg string) {
	Log(WARNING, events.EventIDWarn, mssg)
}

// Fatal adds an event to the event logg with level `ERROR` and quits execution.
func Fatal(err error) {
	Log(ERROR, events.EventIDError, err)
	log.Fatal("Quitting bot execution as fatal event occurred")
}

// Log adds a LogEvent with given code and message to the event logg.
func Log(level Level, id events.EventID, body interface{}) {
	event := &LogEvent{
		BotEvent: events.BotEvent{
			Type: events.LogEventType,
			ID:   id,
			Body: body,
		},
		Level: level,
	}
	logg.input <- event
}

func (l *logger) addListener(listener LogEventListener) {
	l.lock.Lock()
	for _, cached := range l.lastMessages {
		listener(cached)
	}
	l.listeners = append(l.listeners, listener)
	l.lock.Unlock()
}

func (l *logger) close() {
	close(l.fileout)
	close(l.input)
}

// AddListener adds a handler to the event logg.
func AddListener(listener LogEventListener) {
	logg.addListener(listener)
}

// Close flushes and closes the global logger.
func Close() {
	logg.close()
}

package pipeline

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"time"
)

// Limiter adjusts message input to limits in a specific timespan.
// The problem of limiting is solved by periodically sending
// one event incoming to the output where the time of a period depends on the
// maximum number of messages (limit) in a given duration.
func Limiter(done <-chan struct{}, in <-chan *messages.Message, duration time.Duration, limit int) <-chan *messages.Message {
	// No buffers as the sender
	out := make(chan *messages.Message, limit)

	go func() {
		defer close(out)
		timeout := duration / time.Duration(limit)
		for range time.NewTicker(timeout).C {
			select {
			case mssg, ok := <-in:
				if !ok {
					break
				}
				out <- mssg
			case <-done:
				return
			}
		}
	}()

	return out
}

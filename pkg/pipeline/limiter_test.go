package pipeline

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
	"testing"
	"time"
)

func TestLimiter_Close(t *testing.T) {
	input := make(chan *messages.Message, 45)
	done := make(chan struct{})
	defer close(input)

	out := Limiter(done, input, 500*time.Millisecond, 2)

	close(done)
	select {
	case _, more := <-out:
		if more {
			t.Fatal("Not properly closing output")
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out before close")
	}
}

func TestLimiter_limits(t *testing.T) {
	in := make(chan *messages.Message, 5)
	done := make(chan struct{})
	defer close(in)
	defer close(done)

	out := Limiter(done, in, 500*time.Millisecond, 2)

	for i := 1; i <= 5; i++ {
		start := time.Now()
		in <- &messages.Message{
			Code:      messagecode.Buh,
			Transport: transport.CHANNEL,
			Target:    fmt.Sprintf("blaaabot %d", i),
			Message:   "D:",
		}
		in <- &messages.Message{
			Code:      messagecode.Buh,
			Transport: transport.CHANNEL,
			Target:    fmt.Sprintf("blaaabot %d%d", i, i),
			Message:   "D:",
		}

		select {
		case <-out:
			select {
			case <-out:
			case <-time.NewTimer(500 * time.Millisecond).C:
				t.Errorf("Timedout waiting for second output")
				t.Fail()
				return
			}
			took := time.Since(start).Round(time.Millisecond)
			if took >= 505*time.Millisecond || took <= 495*time.Millisecond {
				t.Errorf("Timeout between input and output is not as expected:")
				t.Errorf("Took: %v", took)
				t.Errorf("Actual: %v", 500*time.Millisecond)
				t.Fail()
				return
			}
		case <-time.NewTimer(600 * time.Millisecond).C:
			t.Errorf("Missed message nr. %d", i)
			t.Fail()
		}
	}
}

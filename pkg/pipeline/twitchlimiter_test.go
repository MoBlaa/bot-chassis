package pipeline

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
	"gitlab.com/MoBlaa/bot-chassis/pkg/modes"
	"testing"
	"time"
)

func TestTwitchLimiter_Close(t *testing.T) {
	input := make(chan *messages.Message, 45)
	done := make(chan struct{})
	defer close(input)

	bot := &BotPipeline{
		Cfg: config.Config{
			MessagingConfig: &config.MessagingConfig{
				MessageRateMode: modes.USER,
			},
		},
	}
	out := bot.twitchLimiter(done, input)

	close(done)
	select {
	case _, more := <-out:
		if more {
			t.Fatal("Not properly closing output")
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out before close")
	}
}

func TestFanin_Close(t *testing.T) {
	var inputs []chan *messages.Message
	for i := 0; i < 2; i++ {
		input := make(chan *messages.Message, 45)
		inputs = append(inputs, input)
	}
	defer func() {
		for _, channel := range inputs {
			close(channel)
		}
	}()
	done := make(chan struct{})

	out := fanin(done, inputs[0], inputs[1])

	close(done)
	select {
	case _, more := <-out:
		if more {
			t.Fatal("Not properly closing output")
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out before close")
	}
}

// Tests for whisper account limit being reached
func TestTwitchLimiter_Accounts(t *testing.T) {
	// generate 41 as 40 is the maximum number of accounts to send messages to for USER-Mode
	var mssgs []*messages.Message
	for i := 0; i < 41; i++ {
		mssgs = append(mssgs, &messages.Message{
			Code:      messagecode.Buh,
			Transport: transport.WHISPER,
			Target:    fmt.Sprintf("testuser%d", i+1),
			Message:   "D:",
		})
	}

	in := make(chan *messages.Message, 45)
	done := make(chan struct{})
	defer close(in)
	defer close(done)

	bot := &BotPipeline{
		Cfg: config.Config{
			MessagingConfig: &config.MessagingConfig{
				MessageRateMode: modes.USER,
			},
		},
	}
	out := bot.twitchLimiter(done, in)

	for i, mssg := range mssgs {
		in <- mssg

		// Wait for response and check if 41th message times out
		select {
		case <-out:
		case <-time.NewTimer(time.Second).C:
			if i != 40 {
				// Last one timed out -> success
				t.Fatalf("Response to a message timed out: %d", i)
			}
			return
		}
	}
}

func TestTwitchLimiter_WhisperPerSecond(t *testing.T) {
	expTimeout := time.Second / time.Duration(3)
	mssgs := []*messages.Message{
		{
			Code:      messagecode.Buh,
			Target:    "testTarget",
			Transport: transport.WHISPER,
			Message:   "D:",
		},
		{
			Code:      messagecode.Buh,
			Target:    "testTarget",
			Transport: transport.WHISPER,
			Message:   "D:",
		},
		{
			Code:      messagecode.Buh,
			Target:    "testTarget",
			Transport: transport.WHISPER,
			Message:   "D:",
		},
	}

	in := make(chan *messages.Message, 5)
	done := make(chan struct{})
	defer close(in)
	defer close(done)

	bot := &BotPipeline{
		Cfg: config.Config{
			MessagingConfig: &config.MessagingConfig{
				MessageRateMode: modes.USER,
			},
		},
	}
	out := bot.twitchLimiter(done, in)

	for i, mssg := range mssgs {
		start := time.Now()
		in <- mssg

		select {
		case <-out:
			took := time.Since(start).Round(time.Millisecond)
			if took < expTimeout {
				t.Fatalf("timeout between messages is to big :: expected: %v, actual: %v", expTimeout, took)
				return
			}
		case <-time.NewTimer(time.Second).C:
			t.Fatalf("Response timed out for message nr: %d", i+1)
			return
		}
	}
}

func TestTwitchLimiter_WhisperPerMinute(t *testing.T) {
	var mssgs []*messages.Message
	for i := 0; i < 101; i++ {
		mssgs = append(mssgs, &messages.Message{
			Code:      messagecode.Buh,
			Target:    "test",
			Transport: transport.WHISPER,
			Message:   "D:",
		})
	}

	in := make(chan *messages.Message, 102)
	done := make(chan struct{})
	defer close(in)
	defer close(done)

	bot := &BotPipeline{
		Cfg: config.Config{
			MessagingConfig: &config.MessagingConfig{
				MessageRateMode: modes.USER,
			},
		},
	}
	out := bot.twitchLimiter(done, in)

	for _, mssg := range mssgs {
		in <- mssg
	}

	start := time.Now()
	for i := 0; i < 101; i++ {
		<-out
	}
	took := time.Since(start).Round(time.Minute)

	t.Logf("Took %v", took)
	if took < time.Minute {
		t.Fatalf("Should take more then a minute for all messages :: actual: %v", took)
	}
}

func TestTwitchLimiter_ChatPer30Seconds(t *testing.T) {
	expTimeout := (30 * time.Second) / time.Duration(20)
	mssgs := []*messages.Message{
		{
			Code:      messagecode.Buh,
			Transport: transport.CHANNEL,
			Target:    "test",
			Message:   "D:",
		}, {
			Code:      messagecode.Buh,
			Transport: transport.CHANNEL,
			Target:    "test",
			Message:   "D:",
		},
	}

	in := make(chan *messages.Message, 5)
	done := make(chan struct{})
	defer close(in)
	defer close(done)

	bot := &BotPipeline{
		Cfg: config.Config{
			MessagingConfig: &config.MessagingConfig{
				MessageRateMode: modes.USER,
			},
		},
	}
	out := bot.twitchLimiter(done, in)

	for i, mssg := range mssgs {
		start := time.Now()
		in <- mssg

		select {
		case <-out:
			took := time.Since(start).Round(100 * time.Millisecond)
			if took < expTimeout {
				t.Fatalf("Timeout between messages to a chat should have a minimum timeout of %v :: actual: %v", expTimeout, took)
				return
			}
		case <-time.NewTimer(10 * time.Second).C:
			t.Fatalf("Message nr. %d timed out", i)
			return
		}
	}
}

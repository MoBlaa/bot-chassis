package pipeline

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"testing"
	"time"
)

func TestDecelerator_Closing(t *testing.T) {
	input := make(chan *messages.Event)
	done := make(chan struct{})
	defer close(input)

	bot := &BotPipeline{
		Cfg: config.Config{
			CommandConfig: &config.CommandConfig{
				GlobalCooldown: time.Second * 5,
			},
		},
	}
	out := bot.decelerator(done, input)

	close(done)
	select {
	case _, more := <-out:
		if more {
			t.Log("received event before closed")
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out before closing")
	}
}

func TestDecelerator_Buffer(t *testing.T) {
	input := make(chan *messages.Event, 10)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	events := []*messages.Event{
		{
			Transport: transport.CHANNEL,
			Target:    "target",
			Sender:    domain.NewUser("0", "sender"),
			Message:   "Message1",
		}, {
			Transport: transport.CHANNEL,
			Target:    "target",
			Sender:    domain.NewUser("0", "sender"),
			Message:   "Message2",
		}, {
			Transport: transport.CHANNEL,
			Target:    "target",
			Sender:    domain.NewUser("0", "sender"),
			Message:   "Message3",
		},
	}

	for _, event := range events {
		input <- event
	}

	bot := &BotPipeline{
		Cfg: config.Config{
			CommandConfig: &config.CommandConfig{
				GlobalCooldown: 500 * time.Millisecond,
			},
		},
	}
	output := bot.decelerator(done, input)

	oEvents := <-output
	if len(oEvents) != 2 {
		t.Errorf("No bundled output. Length is %d", len(oEvents))
		t.Fail()
	}
}

func TestDecelerator_Time(t *testing.T) {
	input := make(chan *messages.Event, 10)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	events := []*messages.Event{
		{
			Transport: transport.CHANNEL,
			Target:    "target",
			Sender:    domain.NewUser("0", "sender"),
			Message:   "Message1",
		}, {
			Transport: transport.CHANNEL,
			Target:    "target",
			Sender:    domain.NewUser("0", "sender"),
			Message:   "Message2",
		}, {
			Transport: transport.CHANNEL,
			Target:    "target",
			Sender:    domain.NewUser("0", "sender"),
			Message:   "Message3",
		},
	}

	start := time.Now()
	for _, event := range events {
		input <- event
	}

	bot := &BotPipeline{
		Cfg: config.Config{
			CommandConfig: &config.CommandConfig{
				GlobalCooldown: 50 * time.Millisecond,
			},
		},
	}
	output := bot.decelerator(done, input)

	<-output
	elapsed := time.Since(start)
	if elapsed.Round(time.Millisecond) < 45*time.Millisecond {
		t.Errorf("Took less time then expected. Expected: %v, Actual: %v", 50*time.Millisecond, elapsed)
		t.Fail()
	}
	if elapsed.Round(time.Millisecond) > 55*time.Millisecond {
		t.Errorf("Took less time then expected. Expected: %v, Actual: %v", 50*time.Millisecond, elapsed)
		t.Fail()
	}
}

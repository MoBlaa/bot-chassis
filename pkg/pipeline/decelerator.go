package pipeline

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"time"
)

// Decelerator periodically reads all buffered events of an input-channel
// and returns them as array.
// This is a pre-step for bundling events.
func (b *BotPipeline) decelerator(done <-chan struct{}, in <-chan *messages.Event) <-chan []*messages.Event {
	out := make(chan []*messages.Event, 10000)

	go func() {
		defer close(out)
		for {
			select {
			case <-done:
				return
			case <-time.NewTicker(b.Cfg.CommandConfig.GlobalCooldown).C:
				next, more := <-in
				if !more {
					return
				}

				// Collect all buffered input events
				var input []*messages.Event
				input = append(input, next)
				// Read buffered values
				for i := 0; i < len(in); i++ {
					next, more := <-in
					if !more {
						break
					}
					input = append(input, next)
				}
				if input != nil {
					out <- input
				}
			}
		}
	}()

	return out
}

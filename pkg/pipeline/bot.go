package pipeline

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"gitlab.com/MoBlaa/bot-chassis/pkg/events"
	"gitlab.com/MoBlaa/bot-chassis/pkg/timeouts"
)

// A BotPipeline is listening on chats for Commands, decelerating&grouping Commands, executes them and limits
// the output to the chat by the constrains of the
type BotPipeline struct {
	Cfg      config.Config
	Tom      timeouts.TimeoutManager
	Commands map[string]CommandFunc
}

// Start the bot. this is not returning until some error occurs. After calling this function the Bot
// listens for Commands and executes them.
func (b *BotPipeline) Start(done <-chan struct{}) error {
	cl, err := client.New(b.Cfg.ConnectionConfig)
	if cl != nil {
		go func() {
			<-done
			_ = cl.Close()
		}()
	}
	if err != nil {
		return fmt.Errorf("error while connecsting to twitch: %v", err)
	}

	decelerated := b.decelerator(done, cl.Listen())
	bundledOut := b.groupBy(done, decelerated)
	execOut := b.executor(done, bundledOut)
	limiterOut := b.twitchLimiter(done, execOut)

	cl.StartSender(done, limiterOut)

	elog.Log(elog.INFO, events.EventIDBotStarted, "Bot has started!")

	return nil
}

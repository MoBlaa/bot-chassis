package pipeline

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/commands"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config/cooldown"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config/permissions"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain/roles"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
	"gitlab.com/MoBlaa/bot-chassis/pkg/timeouts"
	"testing"
	"time"
)

func createTestPipe(cfg config.Config) *BotPipeline {
	cmds := make(map[string]CommandFunc)
	cmds["!buh"] = commands.Buh
	cmds["!test"] = commands.Test
	return &BotPipeline{
		Cfg:      cfg,
		Tom:      timeouts.New(),
		Commands: cmds,
	}
}

func TestExecutor_Close(t *testing.T) {
	input := make(chan *messages.BundledEvent, 10)
	done := make(chan struct{})
	defer close(input)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			GlobalCooldown: 0,
			Cooldowns:      make(map[string]config.CooldownConfig),
		},
	}

	bot := createTestPipe(cfg)
	out := bot.executor(done, input)

	close(done)
	select {
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out before done")
	case _, more := <-out:
		if more {
			t.Fatal("Not closing out properly")
		}
	}
}

func TestExecutor_Scared(t *testing.T) {
	input := make(chan *messages.BundledEvent, 10)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			GlobalCooldown: 0,
			Cooldowns:      make(map[string]config.CooldownConfig),
		},
	}

	bot := createTestPipe(cfg)
	output := bot.executor(done, input)

	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		Command:   "!buh",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Sender:    domain.NewUser("1", "sender"),
				Message:   "!buh",
			},
		},
	}

	var reaction *messages.Message
	select {
	case reaction = <-output:
	case <-time.After(50 * time.Millisecond):
	}
	if reaction == nil {
		t.Errorf("Timeout before return")
		t.Fail()
	} else if reaction.Message != "D:" {
		t.Errorf("Executor is not scared: %v", reaction)
		t.Fail()
	}
}

func BenchmarkExecutor_Register(t *testing.B) {
	input := make(chan *messages.BundledEvent, 10)
	//dao := infrastructure.NewSqliteDao("test.db")revents duplication problems as db is wiped after restart
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			GlobalCooldown: 0,
			Cooldowns:      make(map[string]config.CooldownConfig),
		},
	}

	bot := createTestPipe(cfg)
	output := bot.executor(done, input)

	events := make([]*messages.Event, t.N)
	for i := 0; i < t.N; i++ {
		senderName := fmt.Sprintf("sender%d", i)
		events[i] = &messages.Event{
			Transport: transport.WHISPER,
			Target:    "target",
			Sender:    domain.NewUser(fmt.Sprintf("%d", i+1), senderName),
			Message:   "!register",
		}
	}

	t.ResetTimer()
	input <- &messages.BundledEvent{
		Transport: transport.WHISPER,
		Target:    "target",
		Command:   "!register",
		RawEvents: events,
	}

	select {
	case <-output:
	case <-time.After(10 * time.Second):
	}
}

func TestMultiInput(t *testing.T) {
	out := make(chan *messages.Message, 5)
	messagesIn := []*messages.Message{
		{
			Code:      messagecode.Test,
			Transport: transport.CHANNEL,
			Message:   "Some Message",
			Target:    "target",
		}, {
			Code:      messagecode.Test,
			Transport: transport.CHANNEL,
			Message:   "Some Message 1",
			Target:    "target",
		}, {
			Code:      messagecode.Test,
			Transport: transport.CHANNEL,
			Message:   "Some Message 2",
			Target:    "target",
		},
	}
	in := make(chan *messages.Message, 5)
	for _, mssg := range messagesIn {
		in <- mssg
	}
	close(in)

	multiInput(out, in)

	time.Sleep(50 * time.Millisecond)

	if len(out) != 3 {
		t.Errorf("Should contain %d messages but only contained %d", len(messagesIn), len(out))
		return
	}
}

func TestGlobalCooldown(t *testing.T) {
	input := make(chan *messages.BundledEvent)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	users := []*domain.User{
		{
			ID:   1,
			Name: "Max",
		},
	}

	eventIn := []*messages.BundledEvent{
		{
			Transport: transport.CHANNEL,
			Target:    "target",
			RawEvents: []*messages.Event{
				{
					Transport: transport.CHANNEL,
					Target:    "target",
					Message:   "!buh",
					Sender:    users[0],
				},
			},
			Command: "!buh",
		}, {
			Transport: transport.CHANNEL,
			Target:    "target",
			RawEvents: []*messages.Event{
				{
					Transport: transport.CHANNEL,
					Target:    "target",
					Message:   "!test",
					Sender:    users[0],
				},
			},
			Command: "!test",
		},
	}

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			GlobalCooldown: 50 * time.Millisecond,
			Cooldowns:      make(map[string]config.CooldownConfig),
		},
	}

	bot := createTestPipe(cfg)
	out := bot.executor(done, input)
	start := time.Now()
	input <- eventIn[0]

	select {
	case response := <-out:
		// TODO: Fix messageCodes
		if response.Code != messagecode.Buh {
			t.Fatal("Invalid Response to Buh-Command", response)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out waiting for response to Buh-Command!")
	}

	// Check for cooldown
	input <- eventIn[1]
	select {
	case _, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished!")
		}
		took := time.Since(start)
		if took < 50*time.Millisecond {
			t.Fatal("Took less time than cooldown was configured", cfg.CommandConfig.GlobalCooldown)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out waiting for response")
	}
}

func TestCommandCooldownPerUser(t *testing.T) {
	input := make(chan *messages.BundledEvent)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			GlobalCooldown: 0,
			Cooldowns: map[string]config.CooldownConfig{
				"!buh": {
					Amount: 500 * time.Millisecond,
					Mode:   cooldown.PerUser,
				},
			},
		},
	}

	bot := createTestPipe(cfg)

	users := []*domain.User{
		{
			ID:   1,
			Name: "Max",
		}, {
			ID:   2,
			Name: "Mustermann",
		},
	}

	out := bot.executor(done, input)

	// First trigger
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Bad response for Buh command", response)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Second trigger for cooldown message
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.CommandCooldown {
			t.Fatal("Triggering with same user while cooldown should return a message about the cooldown", response)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Third trigger for cooldown message
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[1],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Triggering command with another user should be successful", response)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Wait for cooldown to be over
	<-time.NewTimer(550 * time.Millisecond).C

	// Third trigger for cooldown message
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Triggering after cooldown should be successful", response)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}
}

func TestCooldownAllUsers(t *testing.T) {
	input := make(chan *messages.BundledEvent)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			GlobalCooldown: 0,
			Cooldowns: map[string]config.CooldownConfig{
				"!buh": {
					Amount: 500 * time.Millisecond,
					Mode:   cooldown.AllUsers,
				},
			},
		},
	}
	bot := createTestPipe(cfg)

	users := []*domain.User{
		{
			ID:   1,
			Name: "Max",
		}, {
			ID:   2,
			Name: "Mustermann",
		},
	}

	out := bot.executor(done, input)

	// First trigger
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Bad response for Buh command", response)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Second trigger for cooldown message
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.CommandCooldown {
			t.Fatal("Triggering with same user while cooldown should return a message about the cooldown for all users", response)
		}
	case <-time.NewTimer(30 * time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Third trigger for cooldown message
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[1],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.CommandCooldown {
			t.Fatal("Triggering with same user while cooldown should return a message about the cooldown for all users", response)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Wait for cooldown to be over
	<-time.NewTimer(550 * time.Millisecond).C

	// Third trigger for cooldown message
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[1],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Triggering after cooldown should be successful", response)
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}
}

func TestPermissionBroadcaster(t *testing.T) {
	input := make(chan *messages.BundledEvent)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			Permissions: map[string]config.PermissionsConfig{
				"!buh": {
					Mode:     permissions.StreamerOnly,
					Selected: []string{},
				},
			},
		},
	}
	bot := createTestPipe(cfg)

	users := []*domain.User{
		{
			ID:    1,
			Name:  "Max",
			Roles: []roles.UserRole{roles.Streamer},
		}, {
			ID:   2,
			Name: "Mustermann",
		},
	}

	out := bot.executor(done, input)

	// First trigger
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Bad response for Buh command", response)
		}
	case <-time.NewTimer(60 * time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Second trigger with other user
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[1],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if more {
			t.Fatal("response with not closed response channel while command should only trigger for the broadcaster", response)
		}
	case <-time.NewTimer(time.Minute).C:
	}
}

func TestPermissionMods(t *testing.T) {
	input := make(chan *messages.BundledEvent)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			Permissions: map[string]config.PermissionsConfig{
				"!buh": {
					Mode:     permissions.ModsOnly,
					Selected: []string{},
				},
			},
		},
	}
	bot := createTestPipe(cfg)

	users := []*domain.User{
		{
			ID:    1,
			Name:  "Max",
			Roles: []roles.UserRole{roles.Streamer},
		}, {
			ID:    2,
			Name:  "Mustermann",
			Roles: []roles.UserRole{roles.Moderator},
		}, {
			ID:    3,
			Name:  "Peter",
			Roles: []roles.UserRole{},
		},
	}

	out := bot.executor(done, input)

	// First trigger
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Bad response for Buh command", response)
		}
	case <-time.NewTimer(60 * time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Second trigger with other user
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[1],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Bad response for Buh command", response)
		}
	case <-time.NewTimer(60 * time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Second trigger with other user
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[2],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if more {
			t.Fatal("response with not closed response channel while command should only trigger for the broadcaster", response)
		}
	case <-time.NewTimer(time.Minute).C:
	}
}

func TestPermissionPermitted(t *testing.T) {
	input := make(chan *messages.BundledEvent, 10)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			Permissions: map[string]config.PermissionsConfig{
				"!buh": {
					Mode:     permissions.Selected,
					Selected: []string{"Peter"},
				},
			},
		},
	}
	bot := createTestPipe(cfg)

	users := []*domain.User{
		{
			ID:    1,
			Name:  "Max",
			Roles: []roles.UserRole{roles.Streamer},
		}, {
			ID:    2,
			Name:  "Mustermann",
			Roles: []roles.UserRole{roles.Moderator},
		}, {
			ID:    3,
			Name:  "Peter",
			Roles: []roles.UserRole{},
		}, {
			ID:    4,
			Name:  "Parker",
			Roles: []roles.UserRole{},
		},
	}

	out := bot.executor(done, input)

	// First trigger
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("executor shouldn't close", response)
		}
		if response.Code != messagecode.Buh {
			t.Fatal("bad response to buh command")
		}
	case <-time.NewTimer(20 * time.Millisecond).C:
	}

	// Second trigger with other user
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[1],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("executor shouldn't close", response)
		}
		if response.Code != messagecode.Buh {
			t.Fatal("bad response to buh command")
		}
	case <-time.NewTimer(10 * time.Millisecond).C:
	}

	// Second trigger with other user
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[2],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Bad response for Buh command", response)
		}
	case <-time.NewTimer(30 * time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Third trigger with other user
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[3],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if more {
			t.Fatal("response with not closed response channel while command should only trigger for the broadcaster", response)
		}
	case <-time.NewTimer(10 * time.Millisecond).C:
	}
}

func TestPermissionMultipleCommands(t *testing.T) {
	input := make(chan *messages.BundledEvent)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	cfg := config.Config{
		CommandConfig: &config.CommandConfig{
			Permissions: map[string]config.PermissionsConfig{
				"!buh": {
					Mode:     permissions.StreamerOnly,
					Selected: []string{},
				},
			},
		},
	}
	bot := createTestPipe(cfg)

	users := []*domain.User{
		{
			ID:    1,
			Name:  "Max",
			Roles: []roles.UserRole{roles.Streamer},
		}, {
			ID:   2,
			Name: "Mustermann",
		},
	}

	out := bot.executor(done, input)

	// First trigger
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!buh",
				Sender:    users[0],
			},
		},
		Command: "!buh",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Buh {
			t.Fatal("Bad response for Buh command", response)
		}
	case <-time.NewTimer(60 * time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}

	// Second trigger with other user
	input <- &messages.BundledEvent{
		Transport: transport.CHANNEL,
		Target:    "target",
		RawEvents: []*messages.Event{
			{
				Transport: transport.CHANNEL,
				Target:    "target",
				Message:   "!test",
				Sender:    users[1],
			},
		},
		Command: "!test",
	}
	select {
	case response, more := <-out:
		if !more {
			t.Fatal("Closed output before test finished")
		}
		if response.Code != messagecode.Test {
			t.Fatal("Bad response for test command", response)
		}
	case <-time.NewTimer(60 * time.Second).C:
		t.Fatal("Timed out getting response from executor")
	}
}

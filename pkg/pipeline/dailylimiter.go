package pipeline

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
)

// DailyLimiter limits the amount of accounts the channel can emit messages to.
// Expects the incoming messages to be whispers and be targeted to a username.
func DailyLimiter(done <-chan struct{}, in <-chan *messages.Message, clock Clock, limit int) <-chan *messages.Message {
	out := make(chan *messages.Message, limit+1)

	go func() {
		defer close(out)
		contactedAccounts := make(map[string]struct{})
		for {
			select {
			case mssg, more := <-in:
				if !more {
					return
				}
				if clock.DaySwitched() {
					// Reset records of sent targets if day changes
					contactedAccounts = make(map[string]struct{})
				}

				if _, contained := contactedAccounts[mssg.Target]; !contained && len(contactedAccounts) >= limit {
					// Output, that limit was reached and discard message
					elog.Warn(fmt.Sprintf("Reached limit of unique users to send whispers to. Discarding message sent to: %s\n", mssg.Target))
				} else {
					// Add target and send message to output
					contactedAccounts[mssg.Target] = struct{}{}
					out <- mssg
				}
			case <-done:
				return
			}
		}
	}()

	return out
}

package pipeline

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
	"sync"
	"testing"
	"time"
)

func TestDailyLimiter_Close(t *testing.T) {
	input := make(chan *messages.Message, 45)
	done := make(chan struct{})
	defer close(input)

	out := DailyLimiter(done, input, NewClock(), 2)

	close(done)
	select {
	case _, more := <-out:
		if more {
			t.Fatal("Not properly closing output")
		}
	case <-time.NewTimer(time.Second).C:
		t.Fatal("Timed out before close")
	}
}

// Test daily limiter limits to given amount
func TestDailyLimiter_limits(t *testing.T) {
	mssgs := []*messages.Message{
		{
			Code:      messagecode.Buh,
			Transport: transport.WHISPER,
			Target:    "one",
			Message:   "D:",
		}, {
			Code:      messagecode.Buh,
			Transport: transport.WHISPER,
			Target:    "two",
			Message:   "D:",
		}, {
			Code:      messagecode.Buh,
			Transport: transport.WHISPER,
			Target:    "three",
			Message:   "D:",
		},
	}

	in := make(chan *messages.Message)
	done := make(chan struct{})
	defer close(in)
	defer close(done)

	out := DailyLimiter(done, in, NewClock(), 2)

	in <- mssgs[0]
	in <- mssgs[1]
	in <- mssgs[2]

	if len(out) != 2 {
		t.Errorf("Should return only <limit> (2) number of messages")
		t.Errorf("Returned: %d", len(out))
		t.Fail()
		return
	}
}

// Used to simulate day switch
type testClock struct {
	lock        *sync.Mutex
	daySwitched bool
}

func (cl testClock) DaySwitched() bool {
	cl.lock.Lock()
	switched := cl.daySwitched
	cl.lock.Unlock()
	return switched
}

// Test daily limiter limits to given amount
func TestDailyLimiter_resetOnDayChange(t *testing.T) {
	mssgs := []*messages.Message{
		{
			Code:      messagecode.Buh,
			Transport: transport.WHISPER,
			Target:    "one",
			Message:   "D:",
		}, {
			Code:      messagecode.Buh,
			Transport: transport.WHISPER,
			Target:    "two",
			Message:   "D:",
		}, {
			Code:      messagecode.Buh,
			Transport: transport.WHISPER,
			Target:    "three",
			Message:   "D:",
		},
	}

	in := make(chan *messages.Message)
	done := make(chan struct{})
	defer close(in)
	defer close(done)

	clock := &testClock{
		lock:        &sync.Mutex{},
		daySwitched: false,
	}
	out := DailyLimiter(done, in, clock, 2)

	in <- mssgs[0]
	in <- mssgs[1]

	var out1 *messages.Message
	var out2 *messages.Message
	select {
	case out1 = <-out:
	case <-time.NewTimer(20 * time.Millisecond).C:
		t.Errorf("Timed out waiting for output")
		t.Fail()
		return
	}
	select {
	case out2 = <-out:
	case <-time.NewTimer(20 * time.Millisecond).C:
		t.Errorf("Timed out waiting for output")
		t.Fail()
		return
	}

	if *out1 != *mssgs[0] || *out2 != *mssgs[1] {
		t.Errorf("Should keep order of returned messages")
		t.Errorf("Expected: %v", []*messages.Message{mssgs[0], mssgs[1]})
		t.Errorf("Actual:   %v", []*messages.Message{out1, out2})
		t.Fail()
		return
	}

	clock.lock.Lock()
	clock.daySwitched = true
	clock.lock.Unlock()

	// Write again and expect to return
	in <- mssgs[2]

	select {
	case <-out:
	case <-time.NewTimer(20 * time.Millisecond).C:
		t.Errorf("Should reset limits on day switch!")
		t.Errorf("Output-len: %d", len(out))
		t.Fail()
	}
}

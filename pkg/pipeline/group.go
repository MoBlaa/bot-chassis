package pipeline

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
)

type bundleKey struct {
	Transport transport.Transport
	Target    string
	Command   string
}

func bundle(events []*messages.Event) *messages.BundledEvent {
	return &messages.BundledEvent{
		Transport: events[0].Transport,
		Target:    events[0].Target,
		RawEvents: events,
		Command:   ExtractCommand(events[0]),
	}
}

func mapper(event *messages.Event) bundleKey {
	return bundleKey{
		Transport: event.Transport,
		Target:    event.Target,
		Command:   ExtractCommand(event),
	}
}

// GroupBy splits an array of events into groups identified by Transport,
// Target and Command the events contain.
func (b *BotPipeline) groupBy(done <-chan struct{}, in <-chan []*messages.Event) <-chan *messages.BundledEvent {
	out := make(chan *messages.BundledEvent, 10000)

	go func() {
		defer close(out)
		for {
			// A Map from interface to slice of interfaces
			select {
			case <-done:
				return
			case input, more := <-in:
				if !more {
					return
				}
				grouped := make(map[bundleKey][]*messages.Event)
				// Add elements to map to group them
				for _, event := range input {
					if event == nil {
						continue
					}
					key := mapper(event)
					if _, ok := grouped[key]; !ok {
						// Initialize slice first
						grouped[key] = []*messages.Event{}
					}
					// Append to existing slice
					grouped[key] = append(grouped[key], event)
				}
				// write bundled groups to output
				for _, value := range grouped {
					out <- bundle(value)
				}
			}
		}
	}()

	return out
}

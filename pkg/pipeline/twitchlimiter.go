package pipeline

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"sync"
	"time"
)

// TwitchLimiter adjusts message output to message limits of Twitch.
// These are categorized as:
// - whispers to accounts per day
// - whisper per second
// - whisper per minute
// - chat message per 30 seconds
func (b *BotPipeline) twitchLimiter(done <-chan struct{}, in <-chan *messages.Message) <-chan *messages.Message {
	whispers := make(chan *messages.Message)
	chats := make(chan *messages.Message)
	mode := b.Cfg.MessagingConfig.MessageRateMode

	// Split the input into whisper and channel messages
	go func() {
		defer close(whispers)
		defer close(chats)
		for {
			select {
			case <-done:
				return
			case mssg, more := <-in:
				if !more {
					return
				}
				if mssg.Transport == transport.CHANNEL {
					chats <- mssg
				} else if mssg.Transport == transport.WHISPER {
					whispers <- mssg
				}
			}
		}
	}()

	//// Start Limiters
	// Create channel limiting the chat output
	chatOut := Limiter(done, chats, 30*time.Second, mode.ToChatPer30Seconds())
	//// Chain Whisper-limits
	// Limit daily contacted accounts
	whisperAccOut := DailyLimiter(done, whispers, NewClock(), mode.ToWhisperAccountsPerDay())
	// Limit Messages whispered per minute
	whisperMinuteOut := Limiter(done, whisperAccOut, time.Minute, mode.ToWhisperPerMinute())
	// Limit Messages whispered per second
	whisperOut := Limiter(done, whisperMinuteOut, time.Second, mode.ToWhisperPerSecond())

	// Merge output of chatOut and whisperOut
	return fanin(done, chatOut, whisperOut)
}

func fanin(done <-chan struct{}, in ...<-chan *messages.Message) <-chan *messages.Message {
	var wg sync.WaitGroup
	out := make(chan *messages.Message)

	// Reads output of one input channel
	output := func(ch <-chan *messages.Message) {
		defer wg.Done()
		for {
			// Finish if done or input channel is closed
			select {
			case <-done:
				return
			case m, more := <-ch:
				if !more {
					return
				}
				out <- m
			}
		}
	}
	wg.Add(len(in))

	// Start Goroutine per input channel
	for _, ch := range in {
		go output(ch)
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

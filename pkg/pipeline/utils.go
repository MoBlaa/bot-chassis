package pipeline

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"strings"
)

// ExtractCommand returns the first word after the command contained in the
// raw message of one event.
func ExtractCommand(event *messages.Event) string {
	cmd := strings.ToLower(event.Message)
	cmd = strings.Trim(cmd, " ")
	indEndCmd := strings.Index(cmd, " ")
	if indEndCmd > 0 {
		cmd = cmd[:indEndCmd]
	}
	return cmd
}

// A CommandFunc is a function executing a command. This can be a function associated to a struct or
// a simple Function without associations.
// A CommandFunc has to create a goroutine and return a channel to allow delaying of returns.
// This way a command can wait for an answer or another user to send the command
// until the last message is sent.
type CommandFunc func(event *messages.BundledEvent) <-chan *messages.Message

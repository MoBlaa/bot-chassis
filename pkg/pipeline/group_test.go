package pipeline

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"testing"
	"time"
)

func TestGroupBy_Close(t *testing.T) {
	input := make(chan []*messages.Event, 10000)
	done := make(chan struct{})
	defer close(input)

	bot := createTestPipe(config.Config{})
	out := bot.groupBy(done, input)

	close(done)
	select {
	case <-time.NewTimer(time.Second).C:
		t.Fatal("timed out before closed")
		return
	case _, more := <-out:
		if more {
			t.Fatal("Not Closing output on done")
		}
	}
}

func equals(slice []*messages.BundledEvent, elements []*messages.BundledEvent) bool {
	if len(slice) != len(elements) {
		return false
	}
	for i, v := range slice {
		if v.Eq(elements[i]) {
			return false
		}
	}
	return true
}

func TestGroupBy(t *testing.T) {
	input := make(chan []*messages.Event, 10000)
	done := make(chan struct{})
	defer close(input)
	defer close(done)

	events := []*messages.Event{
		{
			Transport: transport.CHANNEL,
			Target:    "target",
			Sender:    domain.NewUser("0", "sender"),
			Message:   "!buh",
		}, {
			Transport: transport.CHANNEL,
			Target:    "target",
			Sender:    domain.NewUser("1", "sender2"),
			Message:   "!buh",
		}, {
			Transport: transport.CHANNEL,
			Target:    "target1",
			Sender:    domain.NewUser("1", "sender2"),
			Message:   "!buh",
		}, {
			Transport: transport.WHISPER,
			Target:    "target",
			Sender:    domain.NewUser("1", "sender2"),
			Message:   "!buh",
		}, {
			Transport: transport.WHISPER,
			Target:    "target2",
			Sender:    domain.NewUser("1", "sender2"),
			Message:   "!buh",
		}, {
			Transport: transport.WHISPER,
			Target:    "target2",
			Sender:    domain.NewUser("1", "sender2"),
			Message:   "!register",
		},
	}

	expected := []*messages.BundledEvent{
		{
			Transport: transport.CHANNEL,
			Target:    "target",
			Command:   "!buh",
			RawEvents: []*messages.Event{
				{
					Transport: transport.CHANNEL,
					Target:    "target",
					Sender:    domain.NewUser("0", "sender"),
					Message:   "!buh",
				},
				{
					Transport: transport.CHANNEL,
					Target:    "target",
					Sender:    domain.NewUser("1", "sender2"),
					Message:   "!buh",
				},
			},
		}, {
			Transport: transport.CHANNEL,
			Target:    "target1",
			Command:   "!buh",
			RawEvents: []*messages.Event{
				{
					Transport: transport.CHANNEL,
					Target:    "target1",
					Sender:    domain.NewUser("1", "sender2"),
					Message:   "!buh",
				},
			},
		}, {
			Transport: transport.WHISPER,
			Target:    "target",
			Command:   "!buh",
			RawEvents: []*messages.Event{
				{
					Transport: transport.WHISPER,
					Target:    "target",
					Sender:    domain.NewUser("1", "sender2"),
					Message:   "!buh",
				},
			},
		}, {
			Transport: transport.WHISPER,
			Target:    "target2",
			Command:   "!buh",
			RawEvents: []*messages.Event{
				{
					Transport: transport.WHISPER,
					Target:    "target2",
					Sender:    domain.NewUser("1", "sender2"),
					Message:   "!buh",
				},
			},
		}, {
			Transport: transport.WHISPER,
			Target:    "target2",
			Command:   "!register",
			RawEvents: []*messages.Event{
				{
					Transport: transport.WHISPER,
					Target:    "target2",
					Sender:    domain.NewUser("1", "sender2"),
					Message:   "!register",
				},
			},
		},
	}

	bot := createTestPipe(config.Config{})
	output := bot.groupBy(done, input)

	input <- events

	var bundles []*messages.BundledEvent
	bundles = append(bundles, <-output)
	bundles = append(bundles, <-output)
	bundles = append(bundles, <-output)
	bundles = append(bundles, <-output)
	bundles = append(bundles, <-output)
	// 1 received, 4 remaining
	if len(bundles) != 5 {
		t.Errorf("Bad amount of bundles. Expected 5, got %d", len(bundles))
		t.Fail()
	}

	if !equals(expected, bundles) {
		t.Errorf("Returned bundles don't equal expected")
		t.Errorf("Expected :: len=%d, %v", len(expected), expected)
		t.Errorf("Actual:  :: len=%d, %v", len(bundles), bundles)
		t.Fail()
	}
}

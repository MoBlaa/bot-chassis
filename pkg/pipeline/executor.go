package pipeline

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/client/transport"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config/cooldown"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config/permissions"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain"
	"gitlab.com/MoBlaa/bot-chassis/pkg/domain/roles"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"gitlab.com/MoBlaa/bot-chassis/pkg/events"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages"
	"gitlab.com/MoBlaa/bot-chassis/pkg/messages/messagecode"
	"gitlab.com/MoBlaa/bot-chassis/pkg/timeouts"
	"strings"
	"sync"
	"time"
)

func multiInput(out chan<- *messages.Message, in <-chan *messages.Message) {
	for mssg := range in {
		out <- mssg
	}
}

// Executor delegates events to the mapped Commands and executes these Commands.
// Also adds Cooldowns for a triggered command.
func (b *BotPipeline) executor(done <-chan struct{}, eventChan <-chan *messages.BundledEvent) <-chan *messages.Message {
	out := make(chan *messages.Message, 1000)
	var wg sync.WaitGroup

	go func() {
		defer func() {
			// Wait for all Commands to be processed
			wg.Wait()
			close(out)
			b.Tom.Close()
		}()
		for {
			select {
			case <-done:
				return
			case event, more := <-eventChan:
				if !more {
					return
				}

				var invalidUsers []*domain.User
				// Check for Cooldown present
				switch b.Cfg.CommandConfig.Cooldowns[event.Command].Mode {
				case cooldown.AllUsers:
					if b.Tom.Has(event.Command) {
						if event.Transport == transport.WHISPER {
							// Send Message to everyone which sent a whisper
							for _, single := range event.RawEvents {
								mssg := fmt.Sprintf("%s still has cooldown for %s", event.Command, b.Tom.GetRemaining(event.Command))
								out <- &messages.Message{
									Code:      messagecode.CommandCooldown,
									Transport: transport.WHISPER,
									Message:   mssg,
									Target:    single.ResponseTarget(),
								}
								elog.Log(elog.WARNING, events.EventIDCommandCooldown, mssg)
							}
						} else {
							mssg := fmt.Sprintf("%s still has cooldown for %s", event.Command, b.Tom.GetRemaining(event.Command))
							// Single Cooldown message to channel
							out <- &messages.Message{
								Code:      messagecode.CommandCooldown,
								Transport: transport.CHANNEL,
								Message:   mssg,
								Target:    event.Target,
							}
							elog.Log(elog.WARNING, events.EventIDCommandCooldown, mssg)
						}
						// Skip command execution
						continue
					}
				case cooldown.PerUser:
					// Check for every User if cooldown for command is present
					for _, raw := range event.RawEvents {
						key := timeouts.UserCommandKey{
							User:    raw.Sender.Name,
							Command: event.Command,
						}
						if b.Tom.Has(key) {
							invalidUsers = append(invalidUsers, raw.Sender)
							mssg := fmt.Sprintf("@%s you still have a cooldown for %s on command %s", raw.Sender.Name, b.Tom.GetRemaining(key), event.Command)
							// Send message to user about cooldown
							out <- &messages.Message{
								Code:      messagecode.CommandCooldown,
								Transport: event.Transport,
								Message:   mssg,
								Target:    raw.ResponseTarget(),
							}
							elog.Log(elog.WARNING, events.EventIDCommandCooldown, mssg)
						}
					}
				default:
				}
				// Check permissions if present
				perms, ok := b.Cfg.CommandConfig.Permissions[event.Command]
				if ok {
					// Check all events
					for _, raw := range event.RawEvents {
						permitted := false
						if permissions.Selected == perms.Mode {
							// Check if user is in permitted list
							for _, permittedUsernames := range perms.Selected {
								if permittedUsernames == raw.Sender.Name {
									permitted = true
									break
								}
							}
						} else {
							// Check all roles of the sender
							for _, role := range raw.Sender.Roles {
								switch perms.Mode {
								case permissions.StreamerOnly:
									if role == roles.Streamer {
										permitted = true
									}
								case permissions.ModsOnly:
									if role == roles.Streamer || role == roles.Moderator {
										permitted = true
									}
								case permissions.SubscribersOnly:
									if role == roles.Subscriber || role == roles.Streamer || role == roles.Moderator {
										permitted = true
									}
								}
								// Break if permitted to use the command
								if permitted {
									break
								}
							}
						}
						if !permitted {
							elog.Log(elog.WARNING, events.EventIDNotPermitted, fmt.Sprintf("@%s is not allowed to issue the command '%s'", raw.Sender.Name, event.Command))
							invalidUsers = append(invalidUsers, raw.Sender)
						}
					}
				}

				// Build new BundledEvent without events on cooldown or not permitted events
				var raws []*messages.Event
				for _, raw := range event.RawEvents {
					inv := false
					for _, invalid := range invalidUsers {
						inv = inv || raw.Sender.Name == invalid.Name
					}
					if !inv {
						raws = append(raws, raw)
					}
				}
				event.RawEvents = raws
				if len(event.RawEvents) == 0 {
					continue
				}

				wg.Add(1)
				// Start another goroutine for multiple Commands to be processed in parallel
				go func() {
					defer wg.Done()
					if cmd, ok := b.Commands[event.Command]; ok {
						elog.Log(elog.INFO, events.EventIDCommandStarted, fmt.Sprintf("Started processing command '%s'", event.Command))
						multiInput(out, cmd(event))
						elog.Log(elog.INFO, events.EventIDCommandFinished, fmt.Sprintf("Finished processing command '%s'", event.Command))
					} else {
						if strings.HasPrefix(event.Command, "!") {
							elog.Log(elog.WARNING, events.EventIDCommandNotSupported, fmt.Sprintf("Unsupported Command: '%s'", event.Command))
							// Skip setting a cooldown
							return
						}
					}
				}()
				// Set Cooldown
				var err error
				switch b.Cfg.CommandConfig.Cooldowns[event.Command].Mode {
				case cooldown.PerUser:
					for _, sender := range event.Senders() {
						key := timeouts.UserCommandKey{
							User:    sender.Name,
							Command: event.Command,
						}
						err = b.Tom.Add(key, b.Tom.Remove, b.Cfg.CommandConfig.Cooldowns[event.Command].Amount)
						if err != nil {
							err = fmt.Errorf("failed to add cooldown for user %s and command %s, %v", sender, event.Command, err)
						}
					}
				case cooldown.AllUsers:
					err = b.Tom.Add(event.Command, b.Tom.Remove, b.Cfg.CommandConfig.Cooldowns[event.Command].Amount)
				}
				if err != nil {
					elog.Fatal(err)
				}

				// Wait Cooldown till listening again
				select {
				case <-done:
					return
				case <-time.NewTimer(b.Cfg.CommandConfig.GlobalCooldown).C:
				}
			}
		}
	}()

	return out
}

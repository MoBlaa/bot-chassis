package cooldown

// Mode represents the scope the Cooldown is applied to.
type Mode string

const (
	// PerUser represents cooldown triggered per user.
	// User A triggers Command and can't trigger till cooldown is over. All other users can still trigger.
	PerUser Mode = "PER_USER"
	// AllUsers represents cooldown triggered for every invocation of the command.
	// User A triggers command and no one can trigger the command till the cooldown is over.
	AllUsers Mode = "ALL_USERS"
)

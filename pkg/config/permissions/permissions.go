package permissions

// Permission describes the access level of a command.
type Permission string

const (
	// StreamerOnly limits the access to a command to only the streamer in the channel itself.
	StreamerOnly Permission = "STREAMER"
	// ModsOnly limits the access to a command to only the streamer and the mods in the channels.
	ModsOnly Permission = "MODS"
	// SubscribersOnly limits the access to a command to only the streamer, mods and subscribers in the channel.
	SubscribersOnly Permission = "SUBSCRIBER"
	// Selected limits the access to a command to the moderators and streamer as well as a given
	// list of usernames.
	Selected Permission = "SELECTED"
)

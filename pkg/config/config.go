package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/MoBlaa/bot-chassis/pkg/auth"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config/cooldown"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config/permissions"
	"gitlab.com/MoBlaa/bot-chassis/pkg/modes"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

var (
	// ConfigsFolder represents the path to the folder containing all configuration files
	// for the bot.
	ConfigsFolder = filepath.FromSlash("./configs/")
	// ConfigsFile represents the global configuration in json representation.
	ConfigsFile = ConfigsFolder + "configuration.json"
)

// Config contains configuration for functions of the bot-chassis.
type Config struct {
	ConnectionConfig  *ConnectionConfig
	MessagingConfig   *MessagingConfig
	CommandConfig     *CommandConfig
	AuthConfig        *AuthConfig
	StaticFilesConfig *StaticFilesConfig
}

// StaticFilesConfig contains configuration for serving the static file for ui in parallel to the Bot.
type StaticFilesConfig struct {
	// RootFolderPath to the folder containing the static files to serve.
	RootFolderPath string
}

// ConnectionConfig contains options relevant for connection establishment to Twitch.
type ConnectionConfig struct {
	// URL represents the url to the host to connect to for messages.
	URL *url.URL `json:"URL,string"`
	// Username to login as the bot.
	Username string
	// Token represents the OAuth-Token for the Username of the bot.
	Token         string
	ChannelConfig *ChannelConfig
}

// AuthConfig contains configuration for authentication of the user interface.
type AuthConfig struct {
	// Authentication represents the authentication required for the UI.
	Authentication auth.Authentication
}

// MarshalJSON marshals an instance of ConnectionConfig by converting the net.URL to a string.
func (cfg *ConnectionConfig) MarshalJSON() ([]byte, error) {
	tmp := struct {
		URL            string
		Username       string
		Token          string
		*ChannelConfig `json:"ChannelConfig"`
	}{
		Username:      cfg.Username,
		Token:         cfg.Token,
		ChannelConfig: cfg.ChannelConfig,
	}
	if cfg.URL != nil {
		tmp.URL = cfg.URL.String()
	}
	return json.Marshal(tmp)
}

// UnmarshalJSON unmarshals an instance of ConnectionConfig by converting
// the string url to an instance of net.URL.
func (cfg *ConnectionConfig) UnmarshalJSON(data []byte) error {
	type Alias ConnectionConfig
	var tmp *struct {
		URL string
		*Alias
	}
	err := json.Unmarshal(data, &tmp)
	if err != nil {
		return err
	}
	cfg.URL, err = url.Parse(tmp.URL)
	cfg.Username = tmp.Username
	cfg.Token = tmp.Token
	cfg.ChannelConfig = tmp.ChannelConfig
	return err
}

// ChannelConfig contains options relevant for the channels the bot is listening to.
type ChannelConfig struct {
	// JoinedChannels contains the channels the bot is actively listening to.
	JoinedChannels []string
}

// MessagingConfig contains configuration for messaging rates, maximum size of messages etc.
type MessagingConfig struct {
	// MessageRateMode configures the maximum amount of messages to send.
	MessageRateMode modes.MessageRateMode
	//MessageSize int
}

// CommandConfig contains Configurations related to commands their cooldowns etc.
// A Cooldown is
type CommandConfig struct {
	// GlobalCooldown configures the time between command triggers for all commands.
	// Amount for every single command. If command `A` is triggered `GlobalCooldown`
	// has to pass before any command can be triggered again.
	// It also configures the delay between handling messages. This adjusts the
	// amount of messages buffered before they are processed
	GlobalCooldown time.Duration
	// Cooldowns configures the time between command triggers of single commands.
	Cooldowns map[string]CooldownConfig
	// Permissions contains the access permissions for commands.
	Permissions map[string]PermissionsConfig
}

// PermissionsConfig contains the mode and selected usernames if required for a command.
type PermissionsConfig struct {
	// Mode of the permission for the command.
	Mode permissions.Permission
	// Selected is empty or contains users allowed to use the command.
	Selected []string
}

// CooldownConfig contains cooldown options for a single command.
type CooldownConfig struct {
	Amount time.Duration
	Mode   cooldown.Mode
}

const (
	connectionURLKey  = "CONNECTION_URL"
	usernameKey       = "UNAME"
	passwordKey       = "PASSWORD"
	channelsKey       = "CHANNELS"
	mssgmodeKey       = "MESSAGES_MODE"
	globalCooldownKey = "GLOBAL_COOLDOWN"
	cooldownsKey      = "COOLDOWNS"
	permissionsKey    = "COMMAND_PERMISSIONS"

	authCodeKey = "AUTHENTICATION_CODE"

	staticFilesKey = "STATIC_FILES_ROOT"

	// DefaultPrefix of the environment variables for the configuration of the bot-chassis.
	DefaultPrefix = "BOT_"
)

// Save to configuration directory.
func Save(config *Config) error {
	parsed, err := json.Marshal(config)
	if err != nil {
		log.Printf("error parsing configuration: %v :: %v", parsed, err)
		return err
	}

	err = ioutil.WriteFile(ConfigsFile, parsed, os.ModePerm)
	if err != nil {
		log.Printf("error saving configuration: %v :: %v", parsed, err)
		return err
	}
	return nil
}

// Load the configuration required for the bot from environment, command line and .env files.
// The name of the environment variables can be configured through a prefix.
func Load(prefix string, filenames ...string) (*Config, error) {
	content, err := ioutil.ReadFile(ConfigsFile)
	if err == nil {
		// File could be read -> ignore command line params and use file content
		var cfg Config
		err = json.Unmarshal(content, &cfg)
		if err != nil {
			log.Printf("Saved configuration found @ '%s' but an error occured while reading, so it's ignored: %v\n", ConfigsFile, err)
		} else {
			// Return config from that
			return &cfg, nil
		}
	}

	if prefix == "" {
		prefix = DefaultPrefix
	}
	// Load environment file before flags
	err = godotenv.Load(filenames...)
	if err != nil {
		return nil, fmt.Errorf("failed to read environment path: %v", err)
	}
	// Read command line flags
	connPtr := flag.String("url", os.Getenv(prefix+connectionURLKey), "URL to login to twitch")
	usrPtr := flag.String("user", os.Getenv(prefix+usernameKey), "username to use to login to twitch")
	passPtr := flag.String("password", os.Getenv(prefix+passwordKey), "password to use to login to twitch")
	channPtr := flag.String("channels", os.Getenv(prefix+channelsKey), "channels to listen to messages to")
	mssgmodePtr := flag.String("mode", os.Getenv(prefix+mssgmodeKey), "messaging mode for the bot to use")
	cooldownsPtr := flag.String("cds", os.Getenv(prefix+cooldownsKey), "cooldowns for single commands with following format: '!<command name>:<duration>:<\"PER_USER\" or \"ALL_USERS\">;...'")
	permissionsPtr := flag.String("perms", os.Getenv(prefix+permissionsKey), "command permissions with following format: '!<command name>:<mode>(:<selected>);...' where <selected> is a comma-seperated list of usernames only required if the mode selected is used")
	authCodePtr := flag.String("auth", os.Getenv(prefix+authCodeKey), "authentication for user interfaces")
	staticFilesPtr := flag.String("static", os.Getenv(prefix+staticFilesKey), "relative path to the static files to serve in parallel. If folder doesn't exist, no files will be served. Default: 'static'")

	globalCooldown, err := time.ParseDuration(os.Getenv(prefix + globalCooldownKey))
	if err != nil {
		log.Printf("failed to parse '%s': '%s' using default global cooldown of 1s", prefix+globalCooldownKey, os.Getenv(prefix+globalCooldownKey))
		globalCooldown = 1 * time.Second
	}
	glblCdPtr := flag.Duration("glblCd", globalCooldown, "cooldown between processing of any commands")

	flag.Parse()

	// Check if folder for StaticFiles exists and is a folder
	var staticFilesDir string
	if *staticFilesPtr == "" {
		staticFilesDir = "static"
	} else {
		staticFilesDir = filepath.FromSlash(*staticFilesPtr)
	}
	if _, err := os.Stat(staticFilesDir); os.IsNotExist(err) {
		log.Printf("folder for static files '%s' doesn't exist. Not serving files", staticFilesDir)
		staticFilesDir = ""
	}

	// Parse authentication
	validator := regexp.MustCompile("[a-zA-Z]{4}-[a-zA-Z]{4}-[a-zA-Z]{4}")
	var authentication *auth.Authentication
	if *authCodePtr != "" && validator.MatchString(*authCodePtr) {
		authentication = &auth.Authentication{
			Token: strings.ReplaceAll(*authCodePtr, "-", ""),
		}
	} else {
		authentication = auth.New()
	}

	// Parse permissions
	permConfigs := make(map[string]PermissionsConfig)
	cmdPermissions := strings.Split(*permissionsPtr, ";")
	for _, singlePermission := range cmdPermissions {
		params := strings.Split(singlePermission, ":")
		if len(params) != 3 {
			log.Fatal(fmt.Errorf("badly formatted permission: %s", singlePermission))
		}
		name := params[0]
		permission := params[1]
		selected := params[2]

		perm := PermissionsConfig{}
		switch strings.ToUpper(permission) {
		case "STREAMER":
			perm.Mode = permissions.StreamerOnly
		case "MODS":
			perm.Mode = permissions.ModsOnly
		default:
			perm.Mode = permissions.Selected
			permitted := strings.Split(selected, ",")
			for i, p := range permitted {
				permitted[i] = strings.ToLower(p)
			}
			perm.Selected = permitted
		}
		permConfigs[name] = perm
	}

	// Parse Message Rates
	var mssgMode modes.MessageRateMode
	switch strings.ToUpper(*mssgmodePtr) {
	case "KNOWN":
		mssgMode = modes.KNOWN
	case "VERIFIED":
		mssgMode = modes.VERIFIED
	case "USER":
		fallthrough
	default:
		mssgMode = modes.USER
	}

	// Parse connection url
	connectionURL, err := url.Parse(*connPtr)
	if err != nil {
		return nil, fmt.Errorf("failed to parse url from '%s': '%s'", connectionURLKey, *connPtr)
	}

	// Parse channels to join
	channels := strings.Split(*channPtr, ",")

	// Parse cooldowns
	cooldowns := make(map[string]CooldownConfig)
	if cooldownsPtr != nil && "" != *cooldownsPtr {
		for _, cmdCd := range strings.Split(*cooldownsPtr, ",") {
			split := strings.Split(cmdCd, ":")
			cmd := split[0]
			amount, err := time.ParseDuration(split[1])
			if err != nil {
				return nil, fmt.Errorf("failed to parse cooldown for command %s: %v", cmd, err)
			}
			var mode cooldown.Mode
			switch strings.ToUpper(split[2]) {
			case "PER_USER":
				mode = cooldown.PerUser
			case "ALL_USERS":
				mode = cooldown.AllUsers
			default:
				return nil, fmt.Errorf("invalid cooldown mode for command %s: %v", cmd, strings.ToUpper(split[2]))
			}
			cooldowns[cmd] = CooldownConfig{
				Amount: amount,
				Mode:   mode,
			}
		}
	}

	cfg := &Config{
		ConnectionConfig: &ConnectionConfig{
			URL:      connectionURL,
			Username: *usrPtr,
			Token:    *passPtr,
			ChannelConfig: &ChannelConfig{
				JoinedChannels: channels,
			},
		},
		MessagingConfig: &MessagingConfig{
			MessageRateMode: mssgMode,
		},
		CommandConfig: &CommandConfig{
			GlobalCooldown: *glblCdPtr,
			Cooldowns:      cooldowns,
			Permissions:    permConfigs,
		},
		AuthConfig: &AuthConfig{
			Authentication: *authentication,
		},
		StaticFilesConfig: &StaticFilesConfig{
			RootFolderPath: staticFilesDir,
		},
	}

	// Save for later retrieval and auth-token not changing after restart
	err = Save(cfg)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

package config

import (
	"encoding/json"
	"log"
	"net/url"
	"testing"
)

// Response is a data transfer object structure which is used to send information
// to clients connecting to the API.
type Wrapper struct {
	Code int
	Body interface{}
}

func TestMarshal(t *testing.T) {
	url, _ := url.Parse("https://google.com")

	config := &Config{
		ConnectionConfig: &ConnectionConfig{
			URL: url,
		},
		MessagingConfig: &MessagingConfig{},
		CommandConfig:   &CommandConfig{},
	}

	ser, err := json.MarshalIndent(config, "", " ")
	if err != nil {
		t.Fatal(err)
	}

	log.Printf("Serialized: %s", string(ser))
}

func TestUnmarshal(t *testing.T) {
	url, _ := url.Parse("https://google.com")

	config := &Wrapper{
		Code: 200,
		Body: &Config{
			ConnectionConfig: &ConnectionConfig{
				URL: url,
			},
			MessagingConfig: &MessagingConfig{},
			CommandConfig:   &CommandConfig{},
		},
	}

	ser, err := json.MarshalIndent(config, "", " ")
	if err != nil {
		t.Fatal(err)
	}

	log.Printf("Marshalled: %s", string(ser))

	var cfg Wrapper
	err = json.Unmarshal(ser, &cfg)
	if err != nil {
		t.Fatal(err)
	}

	log.Printf("Unmarshalled: %+v", cfg)
}

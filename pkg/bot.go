package pkg

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"gitlab.com/MoBlaa/bot-chassis/pkg/auth"
	"gitlab.com/MoBlaa/bot-chassis/pkg/commands"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"gitlab.com/MoBlaa/bot-chassis/pkg/events"
	"gitlab.com/MoBlaa/bot-chassis/pkg/pipeline"
	"gitlab.com/MoBlaa/bot-chassis/pkg/timeouts"
	"gitlab.com/MoBlaa/bot-chassis/pkg/util"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"reflect"
	"strings"
	"sync"
	"syscall"
)

// TokenParamName is the name of the token in the query parameters of http requests.
const TokenParamName = "token"

// A Bot is listening on chats for commands, decelerating&grouping commands, executes them and limits
// the output to the chat by the constrains of the
type Bot interface {
	AddCommand(name string, commandFunc pipeline.CommandFunc)
	Start()
}

type bot struct {
	commands     map[string]pipeline.CommandFunc
	cfg          config.Config
	cfgUnmarshal func(data []byte) (*config.Config, error)

	lock        sync.RWMutex
	api         *api
	connections map[string]*connection
}

// New creates a new instance of the bot using the configuration and a function to unmarshal
// a config from raw bytes.
func New(cfg config.Config, unmarshal func(data []byte) (*config.Config, error)) Bot {
	cmds := make(map[string]pipeline.CommandFunc)
	cmds["!buh"] = commands.Buh
	cmds["!test"] = commands.Test
	b := &bot{
		commands:     cmds,
		cfg:          cfg,
		cfgUnmarshal: unmarshal,
		connections:  map[string]*connection{},
	}

	// Adding handler to permanently listen to bot
	// Listen to log events and send them to connected websockets
	elog.AddListener(func(event interface{}) {
		var data interface{}
		switch o := event.(type) {
		case events.BotEvent:
			data = o
		case *events.BotEvent:
			data = o
		case elog.LogEvent:
			data = o
		case *elog.LogEvent:
			data = o
		case events.CommandResponse:
			data = o
		case *events.CommandResponse:
			data = o
		default:
			log.Printf("unsupported event: %v", event)
			return
		}
		js, err := json.Marshal(data)
		if err != nil {
			// Not Using elog here, as it may result in infinite loop
			log.Println("failed to marshal event to json", data, err)
			return
		}
		b.lock.RLock()
		for _, conn := range b.connections {
			if conn != nil {
				_, err = conn.Write(js)
				if err != nil {
					log.Println("failed to write message to listener", err)
					log.Println("Removing connection from listeners")
					b.lock.RUnlock()
					err := conn.close()
					b.lock.RLock()
					if err != nil {
						log.Println("error closing listener", err)
					}
				}
			}
		}
		b.lock.RUnlock()
	})

	return b
}

func (b *bot) AddCommand(name string, commandFunc pipeline.CommandFunc) {
	b.commands[name] = commandFunc
}

func (b *bot) Start() {
	b.lock.RLock()
	elog.Info("Bot started!")
	elog.Info(fmt.Sprintf("Now open your browser @ 'http://localhost:8080' and enter the Authentication Code '%s'", b.cfg.AuthConfig.Authentication.String()))
	elog.Info("Please keep this window opened! Closing this window will stop the bot.")
	b.lock.RUnlock()

	// Set api
	b.lock.Lock()
	b.api = newAPI(b.cfg, timeouts.New(), b.commands)
	staticFilesDir := b.cfg.StaticFilesConfig.RootFolderPath
	b.lock.Unlock()

	// EventIDStop handling if sigterm is arriving
	sigs := make(chan os.Signal, 1)
	done := make(chan struct{}, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		elog.Warn(fmt.Sprintf("stopping bot execution explicitly with code '%d'", sig))
		close(done)
	}()

	// Check for static files to serve
	staticFilesHandler := func(w http.ResponseWriter, r *http.Request) {
		b.lock.RLock()
		folder := b.cfg.StaticFilesConfig.RootFolderPath
		b.lock.RUnlock()
		if _, err := os.Stat(folder + r.URL.Path); err != nil {
			http.ServeFile(w, r, folder+"/index.html")
			return
		}
		http.ServeFile(w, r, folder+r.URL.Path)
	}

	go func() {
		r := mux.NewRouter()
		if staticFilesDir != "" {
			r.NotFoundHandler = r.NewRoute().HandlerFunc(staticFilesHandler).GetHandler()
		}
		http.Handle("/", r)
		http.HandleFunc("/ws", b.handle)
		err := http.ListenAndServe(":8080", nil)
		if err != nil {
			log.Printf("error serving http: %v", err)
		}
		close(done)
	}()

	<-done
	b.lock.Lock()
	b.api.close()
	b.api = nil
	b.lock.Unlock()
	elog.Close()
}

func (b *bot) handle(w http.ResponseWriter, r *http.Request) {
	// Parse Token
	err := b.parseHTTPToken(r.URL)
	if err != nil {
		writeHTTPResponse(w, HTTPResponse{
			Code: http.StatusBadRequest,
			Body: err.Error(),
		})
		return
	}

	// Upgrade to websocket
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		elog.Error(fmt.Errorf("error upgrading to websocket connection: %w", err))
		return
	}
	connect := &connection{
		conn: conn,
	}

	// Handling connection close
	conn.SetCloseHandler(func(code int, text string) error {
		elog.Info(fmt.Sprintf("closing websocket connection: {code: '%d', mssg: '%s'}", code, text))
		connect.lock.Lock()
		id := connect.id
		connect.lock.Unlock()
		b.removeConnection(id)
		return nil
	})

	// EventIDStart listener for websocket commands
	go func() {
		defer func() {
			err = connect.close()
			if err != nil {
				elog.Error(fmt.Errorf("error closing connection: %w", err))
			}
		}()
		// TODO: Send current state information to new connection
		//  - cached messages (since last bot start) BUG

		//// Send current bot information on connection initialization
		// Read information
		b.lock.RLock()
		botStatus := b.api.getBotStatus()
		b.lock.RUnlock()
		// Write Information to connection
		botInfoEvent := NewBotInfoEvent(&BotInfo{Status: botStatus})
		err = connect.WriteJSON(botInfoEvent)
		if err != nil {
			elog.Error(fmt.Errorf("error writing BotInfo: %w", err))
			return
		}

		// Listen to messages from the UI
		for {
			_, raw, err := connect.read()
			if err != nil {
				elog.Error(fmt.Errorf("error reading message: %w", err))
				return
			}

			// Parse command
			var cmd events.BotEvent
			err = json.Unmarshal(raw, &cmd)
			if err != nil {
				elog.Error(fmt.Errorf("error parsing command:\nraw: %s\nerror: %w", string(raw), err))
				continue
			}

			ser, _ := json.MarshalIndent(cmd, "", " ")
			elog.Info(fmt.Sprintf("received BotCommand: %s", string(ser)))

			// Check if still running
			b.lock.RLock()
			api := b.api
			if api == nil {
				elog.Info("quit listening to connection as api is down")
				b.lock.RUnlock()
				return
			}
			b.lock.RUnlock()
			var response *events.CommandResponse
			switch cmd.ID {
			case events.EventIDGetConfig:
				response = api.getConfig()
			case events.EventIDSetConfig:
				if cfg, ok := cmd.Body.(*config.Config); ok {
					response = api.setConfig(cfg)
				} else {
					elog.Error(fmt.Errorf("unsupported body type in set-config command: %v", reflect.TypeOf(cmd.Body)))
				}
			case events.EventIDStart:
				response = api.start()
			case events.EventIDStop:
				response = api.stop()
			default:
				elog.Log(elog.INFO, events.EventIDCommandNotSupported, fmt.Sprintf("unsupported command: %s", cmd.ID))
			}

			err = connect.WriteJSON(response)
			if err != nil {
				elog.Error(fmt.Errorf("error writing message to websocket connection: %w", err))
				return
			}
		}
	}()

	// Add to connections
	b.addConnection(connect)
}

func (b *bot) addConnection(c *connection) {
	b.lock.Lock()
	id := util.RandomString(24)
	c.id = id
	b.connections[id] = c
	elog.Debug(fmt.Sprintf("added connection. total number of connections: %d\n", len(b.connections)))
	b.lock.Unlock()
}

func (b *bot) removeConnection(target string) {
	b.lock.Lock()
	delete(b.connections, target)
	elog.Debug(fmt.Sprintf("removed connection. total number of connections: %d\n", len(b.connections)))
	b.lock.Unlock()
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	// Simply allow all origins as connection will be checked with a token
	// Should may be moved to 'init()' function
	CheckOrigin: func(request *http.Request) bool {
		return true
	},
}

// HTTPResponse structures the responses of the http api.
type HTTPResponse struct {
	Code    int
	Body    interface{}
	Headers map[string]string `json:"-"`
}

func (b *bot) parseHTTPToken(url *url.URL) error {
	token := url.Query().Get(TokenParamName)
	if token == "" {
		return fmt.Errorf("missing query param '%s'", TokenParamName)
	}

	token = strings.ReplaceAll(token, auth.BlockSeparator, "")

	if b.cfg.AuthConfig.Authentication.Token != token {
		return fmt.Errorf("bad identification token '%s'", token)
	}
	return nil
}

func writeHTTPResponse(w http.ResponseWriter, response HTTPResponse) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if response.Headers != nil {
		for key, val := range response.Headers {
			w.Header().Set(key, val)
		}
	}
	w.WriteHeader(response.Code)
	body, err := json.Marshal(response)
	if err != nil {
		mssg := fmt.Sprint("failed to marshal response body", err)
		elog.Debug(mssg)
		_, err = io.WriteString(w, mssg)
	} else {
		_, err = io.WriteString(w, string(body))
	}
	if err != nil {
		elog.Error(fmt.Errorf("error writing response: %w", err))
	}
}

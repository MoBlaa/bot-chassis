package pkg

import (
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"gitlab.com/MoBlaa/bot-chassis/pkg/events"
)

// BotStatus represents the connection status of the bot.
type BotStatus string

const (
	// Running - Bot has successfully established a connection to the configured channels.
	Running BotStatus = "RUNNING"
	// Stopped - Bot is not listening to any channels.
	Stopped BotStatus = "STOPPED"
)

// BotInfoEvent represents a LogEvent containing the current state information of the bot.
type BotInfoEvent struct {
	elog.LogEvent
	Information *BotInfo
}

// BotInfo represents a summary of the connection state and other status related information.
type BotInfo struct {
	// Current connection status of the bot.
	Status BotStatus
}

// NewBotInfoEvent creates a new LogEvent on INFO Level with the given status Information
func NewBotInfoEvent(info *BotInfo) *BotInfoEvent {
	return &BotInfoEvent{
		LogEvent: elog.LogEvent{
			BotEvent: events.BotEvent{
				Type: events.LogEventType,
				ID:   events.EventIDBotInfo,
			},
			Level: elog.INFO,
		},
		Information: info,
	}
}

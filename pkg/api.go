package pkg

import (
	"fmt"
	"gitlab.com/MoBlaa/bot-chassis/pkg/config"
	"gitlab.com/MoBlaa/bot-chassis/pkg/elog"
	"gitlab.com/MoBlaa/bot-chassis/pkg/events"
	"gitlab.com/MoBlaa/bot-chassis/pkg/pipeline"
	"gitlab.com/MoBlaa/bot-chassis/pkg/timeouts"
	"net/http"
	"sync"
)

// TODO: Add "Update Config" to only update partial configurations and not rely on the frontend to not send incomplete data

// api is implementing the functions the bot is offering for user interfaces.
type api struct {
	cfg  config.Config
	tom  timeouts.TimeoutManager
	cmds map[string]pipeline.CommandFunc

	done chan struct{}

	lock sync.RWMutex
}

func newAPI(cfg config.Config, tom timeouts.TimeoutManager, cmds map[string]pipeline.CommandFunc) *api {
	return &api{
		cfg:  cfg,
		tom:  tom,
		cmds: cmds,
	}
}

func (a *api) getBotStatus() (status BotStatus) {
	a.lock.RLock()
	if a.done == nil {
		status = Stopped
	} else {
		status = Running
	}
	a.lock.RUnlock()
	return
}

// EventIDStart the Bot pipeline.
func (a *api) start() *events.CommandResponse {
	a.lock.RLock()
	alreadyRunning := a.done != nil
	a.lock.RUnlock()
	if alreadyRunning {
		return &events.CommandResponse{
			BotEvent: events.BotEvent{
				Type: events.CommandResponseType,
				ID:   events.EventIDStart,
				Body: fmt.Sprintf("bot is already running"),
			},
			Code: http.StatusBadRequest,
		}
	}

	done := make(chan struct{})
	// Create a new BotPipeline
	a.lock.RLock()
	pipe := pipeline.BotPipeline{
		Cfg:      a.cfg,
		Tom:      a.tom,
		Commands: a.cmds,
	}
	a.lock.RUnlock()

	// EventIDStart the BotPipeline
	err := pipe.Start(done)
	if err != nil {
		return &events.CommandResponse{
			Code: http.StatusBadRequest,
			BotEvent: events.BotEvent{
				Type: events.CommandResponseType,
				ID:   events.EventIDStart,
				Body: err.Error(),
			},
		}
	}
	a.lock.Lock()
	a.done = done
	a.lock.Unlock()

	// Return successfully started bot
	return &events.CommandResponse{
		Code: http.StatusOK,
		BotEvent: events.BotEvent{
			Type: events.CommandResponseType,
			ID:   events.EventIDStart,
			Body: fmt.Sprintf("Successfully started bot!"),
		},
	}
}

// EventIDStop the Bot. Requires the identification token in the request parameters.
func (a *api) stop() *events.CommandResponse {
	// EventIDStop the Bot
	a.lock.Lock()
	if a.done != nil {
		elog.Log(elog.INFO, events.EventIDBotFinalizing, "Closing bot execution!")
		close(a.done)
		a.done = nil
	}
	a.lock.Unlock()

	// Write response
	return &events.CommandResponse{
		Code: http.StatusOK,
		BotEvent: events.BotEvent{
			Type: events.CommandResponseType,
			ID:   events.EventIDStop,
		},
	}
}

// EventIDSetConfig sets the current config to the given parameters.
// Requires a config object in the request body.
func (a *api) setConfig(cfg *config.Config) *events.CommandResponse {
	// Set current config
	a.lock.Lock()
	a.cfg = *cfg
	err := config.Save(cfg)
	a.lock.Unlock()
	if err != nil {
		return &events.CommandResponse{
			Code: http.StatusInternalServerError,
			BotEvent: events.BotEvent{
				Type: events.CommandResponseType,
				ID:   events.EventIDSetConfig,
				Body: fmt.Sprint("failed saving configuration", err),
			},
		}
	}

	// Return
	return &events.CommandResponse{
		Code: http.StatusOK,
		BotEvent: events.BotEvent{
			Type: events.CommandResponseType,
			ID:   events.EventIDSetConfig,
			Body: "Successfully set configuration",
		},
	}
}

// EventIDGetConfig fetches the current configuration of the bot.
// If the bot is not initialized in any way this returns the default values for the configurations.
func (a *api) getConfig() *events.CommandResponse {
	// Read Config
	a.lock.RLock()
	cfg := a.cfg
	a.lock.RUnlock()

	// Write the current configuration to the response
	return &events.CommandResponse{
		Code: http.StatusOK,
		BotEvent: events.BotEvent{
			Type: events.CommandResponseType,
			ID:   events.EventIDGetConfig,
			Body: cfg,
		},
	}
}

// Close finishes up any data present. This closes the running bot if present.
func (a *api) close() {
	a.lock.Lock()
	if a.done != nil {
		close(a.done)
	}
	a.lock.Unlock()
}

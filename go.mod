module gitlab.com/MoBlaa/bot-chassis

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.1
	github.com/joho/godotenv v1.3.0
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f // indirect
)
